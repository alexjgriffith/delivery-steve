{:data {:ground {:data [[1
                         1
                         1
                         1
                         1
                         1
                         1
                         1
                         1
                         1
                         1
                         1]
                        [1
                         2
                         2
                         1
                         1
                         1
                         1
                         1
                         1
                         1
                         1
                         1]
                        [1
                         2
                         2
                         2
                         2
                         2
                         2
                         2
                         2
                         1
                         1
                         1]
                        [1
                         2
                         2
                         2
                         2
                         2
                         2
                         2
                         2
                         2
                         2
                         1]
                        [1
                         1
                         1
                         1
                         1
                         1
                         2
                         2
                         2
                         2
                         2
                         1]
                        [1
                         1
                         1
                         1
                         1
                         1
                         1
                         2
                         2
                         2
                         2
                         1]
                        [1
                         1
                         1
                         2
                         2
                         1
                         1
                         2
                         2
                         2
                         2
                         1]
                        [1
                         1
                         1
                         2
                         2
                         1
                         1
                         2
                         2
                         2
                         2
                         1]
                        [1
                         1
                         1
                         2
                         2
                         1
                         1
                         2
                         2
                         2
                         2
                         1]
                        [1
                         1
                         1
                         2
                         2
                         1
                         1
                         2
                         2
                         2
                         2
                         1]
                        [1
                         1
                         2
                         2
                         2
                         1
                         1
                         2
                         2
                         2
                         2
                         1]
                        [1
                         2
                         2
                         2
                         2
                         1
                         1
                         2
                         2
                         2
                         2
                         1]
                        [1
                         2
                         2
                         2
                         2
                         1
                         1
                         2
                         2
                         2
                         2
                         1]
                        [1
                         2
                         2
                         2
                         2
                         1
                         1
                         2
                         2
                         2
                         2
                         1]
                        [1
                         2
                         2
                         2
                         2
                         1
                         1
                         2
                         2
                         2
                         2
                         1]
                        [1
                         2
                         2
                         2
                         2
                         1
                         1
                         2
                         2
                         2
                         2
                         1]
                        [1
                         1
                         2
                         2
                         2
                         1
                         1
                         2
                         2
                         2
                         2
                         1]
                        [1
                         1
                         2
                         2
                         2
                         1
                         1
                         2
                         2
                         2
                         2
                         1]
                        [1
                         1
                         2
                         2
                         2
                         1
                         1
                         2
                         2
                         2
                         2
                         1]
                        [1
                         1
                         2
                         2
                         2
                         1
                         1
                         2
                         2
                         2
                         2
                         1]
                        [1
                         1
                         2
                         2
                         2
                         1
                         1
                         2
                         2
                         2
                         2
                         1]
                        [1
                         1
                         2
                         2
                         2
                         1
                         1
                         2
                         2
                         2
                         2
                         1]
                        [1
                         1
                         1
                         1
                         1
                         1
                         1
                         2
                         2
                         2
                         2
                         1]
                        [1
                         1
                         1
                         1
                         1
                         1
                         2
                         2
                         2
                         2
                         2
                         1]
                        [1
                         2
                         2
                         2
                         2
                         2
                         2
                         2
                         2
                         2
                         2
                         1]
                        [1
                         2
                         2
                         2
                         2
                         2
                         2
                         2
                         2
                         1
                         1
                         1]
                        [1
                         2
                         2
                         1
                         1
                         1
                         1
                         1
                         1
                         1
                         1
                         1]
                        [1
                         1
                         1
                         1
                         1
                         1
                         1
                         1
                         1
                         1
                         1
                         1]]
                 :map ["chasm" "ground"]
                 :off-x -10
                 :off-y 16
                 :range {:maxx 18 :maxy 28 :minx -9 :miny 17}
                 :type "dense"}
        :objects [{:h 80
                   :type "post-office"
                   :unique true
                   :w 160
                   :x 224
                   :y 512}
                  {:chim [57 10]
                   :h 80
                   :name "carl"
                   :type "houses"
                   :unique true
                   :w 80
                   :x -48
                   :y 560}
                  {:h 40
                   :name "carl"
                   :type "characters"
                   :unique true
                   :w 16
                   :x 0
                   :y 624}
                  {:h 40
                   :name "hank"
                   :type "characters"
                   :unique true
                   :w 16
                   :x 32
                   :y 592}
                  {:h 40
                   :name "tom"
                   :type "characters"
                   :unique true
                   :w 16
                   :x 304
                   :y 592}
                  {:h 40
                   :name "sara"
                   :type "characters"
                   :unique true
                   :w 16
                   :x 80
                   :y 560}
                  {:h 40
                   :name "jill"
                   :type "characters"
                   :unique true
                   :w 16
                   :x 192
                   :y 560}
                  {:h 40
                   :name "k"
                   :type "characters"
                   :unique true
                   :w 16
                   :x 240
                   :y 592}
                  {:h 40
                   :name "aly"
                   :type "characters"
                   :unique true
                   :w 16
                   :x -48
                   :y 624}
                  {:number "sara"
                   :type "pack"
                   :unique true
                   :x 80
                   :y 560}
                  {:h 40
                   :name "mike"
                   :type "characters"
                   :unique true
                   :w 16
                   :x 336
                   :y 592}
                  {:__index 13
                   :h 336
                   :number 2
                   :type "text"
                   :unique true
                   :w 200
                   :x -32
                   :y 432}
                  {:__index 14
                   :h 336
                   :number 3
                   :type "text"
                   :unique true
                   :w 200
                   :x -32
                   :y 464}
                  {:__index 15
                   :h 336
                   :number 4
                   :type "text"
                   :unique true
                   :w 200
                   :x -32
                   :y 496}
                  {:h 336
                   :number 5
                   :type "text"
                   :unique true
                   :w 200
                   :x -16
                   :y 656}
                  {:number "k"
                   :type "pack"
                   :unique true
                   :x 112
                   :y 576}
                  {:number "jill"
                   :type "pack"
                   :unique true
                   :x 192
                   :y 560}
                  {:number "aly"
                   :type "pack"
                   :unique true
                   :x 80
                   :y 560}]
        :players {:steve {:flipped 1 :name "steve" :x 151.9908753401383 :y 550}}}
 :height 40
 :id 0
 :tile-set {:chasm {:auto "blob"
                    :collidable "fall"
                    :layer "ground"
                    :pos [3 1]
                    :size "square4"}
            :ground {:auto "blob"
                     :collidable "fall"
                     :layer "ground"
                     :pos [1 1]
                     :size "square10"}}
 :tile-size 32
 :width 40}