{:data {:ground {:data [[1 1 1 1 1 1]
                        [1 2 2 2 2 1]
                        [1 2 2 2 2 1]
                        [1 2 2 2 2 1]
                        [1 2 2 2 2 1]
                        [1 2 2 2 2 1]
                        [1 1 1 1 1 1]]
                 :map ["chasm" "ground"]
                 :off-x 2
                 :off-y 18
                 :range {:maxx 9 :maxy 24 :minx 3 :miny 19}
                 :type "dense"}
        :objects {}
        :players {:steve {:name "steve" :x 160 :y 320}}}
 :height 40
 :id 0
 :tile-set {:chasm {:auto "blob"
                    :collidable "fall"
                    :layer "ground"
                    :pos [3 1]
                    :size "square4"}
            :ground {:auto "blob"
                     :collidable "fall"
                     :layer "ground"
                     :pos [1 1]
                     :size "square10"}}
 :tile-size 32
 :width 40}