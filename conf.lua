love.conf = function(t)
   t.gammacorrect = true
   t.title, t.identity = "delivery-steve", "delivery-steve"
   t.modules.joystick = false
   t.modules.physics = false
   t.window.width = 1280
   t.window.height = 720
   t.window.vsync = true
   t.version = "11.3"
   t.window.fullscreentype = "desktop"-- "desktop" -- "exclusive"
   t.window.fullscreen = false
   t.window.minwidth = 1280
   t.window.minheight = 720
   t.window.resizable = false
   t.window.centered = true

   -- t.window.x = 0 -- Make sure to unset this
   -- t.window.y = 0 -- Make sure to unset this
end
