(local objects {})

(local loader (require :lib.loader))

(fn objects.sort [a b]
  (local order {
                :tiles 1
                :text 2
                :houses 3
                :post-office 4
                :decals 5
                :characters 6
                :player 7
                :packs 8})
    (let [va (or (. order a.type) 0)
          vb (or (. order b.type) 0)]
      (> va vb)))

(fn add-pack [pack sprite-atlas new-object ret]
  (local state (require :state))
  (local objs state.objects)
  (var held false)
  (when objs
    (each [_ obj (pairs objs)]
    (when (and (not held)
               obj.type (= obj.type :characters)
               (< (lume.distance new-object.x new-object.y obj.pos.x obj.pos.y) 10))
      (table.insert obj.packages new-object.number)
      (set held true)
      )
    ))
  (each [_ obj (pairs ret)]
    (when (and (not held)
               obj.type (= obj.type :characters)
               (< (lume.distance new-object.x new-object.y obj.pos.x obj.pos.y) 30))
      (table.insert obj.packages new-object.number)
      (set held true)
      ))

    (tset ret (.. :package "-" new-object.number)
          (pack.new sprite-atlas new-object.number new-object.x new-object.y held))

  )

(fn objects.pickup-packs [ret]
  (each [key pack (pairs ret)]
    (when (and pack.type (= pack.type :pack))
      (var held false)
      (tset pack :held? false)
      (each [key obj (pairs ret)]
        (when (and (not held)
                   obj.type
                   (= obj.type :characters)
                   (< (lume.distance pack.pos.x pack.pos.y obj.pos.x obj.pos.y) 30))
          (table.insert obj.packages pack.number)
          (tset obj :packages (lume.unique obj.packages))
          (set held true)
          (tset pack :held? held))))))

(fn objects.gen-objects [objs]
  (local state (require :state))
  (local characters (require :prefab-characters))
  (local houses (require :prefab-houses))
  (local post-office (require :prefab-post-office))
  (local pack (require :prefab-pack))
  (local text (require :prefab-text))
  (local house-atlas (loader.load-atlas "assets/data/house-data" "assets/sprites"))
  (local sprite-atlas (loader.load-atlas "assets/data/sprite-data" "assets/sprites"))
  (local ret {})
  (each [_ object (pairs objs)]
    (match object.type
      :characters (tset ret (.. :character "-" object.name)
                        (characters.new sprite-atlas object.name object.x object.y))
      :houses (do
                (tset ret (.. :house "-" object.name)
                 (houses.new house-atlas object.name object.x object.y object.chim)))
      :post-office (tset ret  :post-office  (post-office.new house-atlas object.x object.y))
      :text (let [text-obj (text.new house-atlas state.current-level object.number object.x object.y)]
              (when text-obj
                (tset ret (.. :text- state.current-level "-" object.number)
                      text-obj)))
      :pack (tset ret (.. :package "-" object.number)
          (pack.new sprite-atlas object.number object.x object.y false))
      :player (db "can't make player")
      ))
  (table.sort ret objects.sort)
  (objects.pickup-packs ret)
  ret
  )

(fn objects.gen-object [object]
  (var ret {})
  (each [name value (pairs (objects.gen-objects [object]))]
    (set ret value))
  ret)

(fn objects.gen-icons [objs]
  (local (_w h _flags) (love.window.getMode))
  (local w 1280)
  (local icon (require :prefab-portrait))
  (local ret {})
  (local y 4);;(- h 32 4))
  (var x-decal 4)
  (var x-coin (- w 32 4))
  (each [_ object (pairs objs)]
    (match object.type
      :portrait (do (tset ret (.. :portrait " " object.name)
                      (icon.new object.name x-coin y))
                (set x-coin (- x-coin 4 42)))
      ))
  ret
  )


objects
