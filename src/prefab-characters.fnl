(local characters {})

(local emote (require :emote))

(fn characters.collide [characters])


(local animations {:talk {:frames [1 2] :rate 0.300}
                   :idle {:frames [3 4] :rate 0.300}
                   :wave {:frames [5 6] :rate 0.300}})

(fn get-state [self]
  (local steve (. (require :state) :steve))
  (let [distance (lume.distance (- self.pos.x 8) self.pos.y
                                steve.pos.x steve.pos.y)]
    (match [distance]
      ([d] ? (< d 32))  :talk
      ([d] ? (< d 80))  :wave
      [_] :idle
      )))

(fn check-flipped [self]
  (local steve (. (require :state) :steve))
  (if (> (- steve.pos.x self.pos.x) 0)
      -1
      1))

(fn get-package-name [self]
  (var name nil)
  (when (> (# self.packages) 0)
    (if (~= self.short-name (. self.packages 1))
        (set name (. self.packages 1))
        (> (# self.packages) 1)
        (set name (. self.packages 2)))
    name))

(fn show-package [self]
    (local state (require :state))
    (local steve state.steve)
    (local d1 (lume.distance (- steve.pos.x 8) steve.pos.y self.pos.x self.pos.y))
    (local name (get-package-name self))
    (if (and name (< d1 40))
        (tset self :showing (. self.name-map name))
        (tset self :showing 0)))


(fn package-exchange [self wants-package has-package]
  (fn [player]
    (or (self:give-package player.packages)
        (self:take-package player.packages))
    )
  )

(fn update-emotes [self]
  (local state (require :state))
  (local steve state.steve)
  (local d1 (lume.distance (- steve.pos.x 8) steve.pos.y self.pos.x self.pos.y))
  (local d2 (lume.distance (+ steve.speed.x -8 steve.pos.x) (+ steve.speed.y steve.pos.y) self.pos.x self.pos.y))
  (local approaching (<= (- d2 d1) 0))
   (local has-package (if (self:has-package) true))
  (local wants-package (not (. (lume.invert self.packages) self.short-name)))
  (match [d1 approaching has-package wants-package]
    ([d true  true  _    ] ? (< d 640)) (self.emotes:set :ex true)
    ([d false true  _    ] ? (< d 320)) (self.emotes:set :question true)
    ([d _     _     false] ? (< d 64)) (self.emotes:set :happy true)
    ([d _     _     true ] ? (< d 64)) (self.emotes:set :sad true)
    _                                  (self.emotes:end)
    )

  (if (or wants-package has-package)
      (tset self :action
            (package-exchange self wants-package has-package))
      (tset self :action nil))
  )



(fn update-animation-state [self dt]
  (local new-state (get-state self))
  (local flipped (check-flipped self))
  (tset self.animation :flipped flipped)
  (tset self.animation :timer (+ self.animation.timer dt))
  (when (>  self.animation.timer self.animation.period)
    (tset self.animation :timer 0)
    (tset self.animation :index (+ self.animation.index 1)))
  (when (> self.animation.index (# self.animation.frames))
    (tset self.animation :index 1))
  (when (~= new-state self.animation.animation-state)
    (tset self.animation :timer 0)
    (tset self.animation :index 1)
    (tset self.animation :animation-state new-state)
    (tset self.animation :period (. animations new-state :rate))
    (tset self.animation :frames (. animations new-state :frames))))

(fn characters.update [self dt]
  (show-package self)
  (update-emotes self)
  (self.emotes:update dt self)
  (update-animation-state self dt))


(fn characters.draw [self pos? editor?]
  (love.graphics.setColor 1 1 1 1)
  (local {: index : frames} self.animation)
  (local quad (. self.characters-quads (. frames index)))
  (if (= :editor editor?)
      (do (love.graphics.draw self.image quad 9 -12 0 2))
      (let [xc (if (= -1 self.animation.flipped)
                  (+ self.root.x self.pos.x 20)
                  (+ self.root.x self.pos.x))
            xs (if (= -1 self.animation.flipped)
                  (+ self.root.x self.pos.x 4)
                  (+ self.root.x self.pos.x))
                  ]
        (love.graphics.setColor 0.5 0.5 1 0.3)
        (love.graphics.draw self.image self.shadow-quad
                            xs
                            (+ self.root.y self.pos.y 1 17))
        ;;(love.graphics.printf self.animation.animation-state self.pos.x (- self.pos.y 16) 200)
        (love.graphics.setColor 1 1 1 1)
        (love.graphics.draw self.image quad
                            xc
                            (+ self.root.y self.pos.y)
                            0 self.animation.flipped 1)
        (when (~= self.showing 0)
            (love.graphics.draw self.image
                            (. self.portrait-quads self.showing)
                            (+ self.root.x self.pos.x)
                            (+ self.root.y self.pos.y -30)
                            0
                            1.3))
        (when self.emotes.active
          (tset self.emotes :flipped self.animation.flipped)
          ;; (tset self.emotes.pos :x xc)
          (self.emotes:draw))


        )))

(local characters-mt {:__index characters
                :update characters.update
                :collide characters.collide
                :draw characters.draw})

(fn characters.give-package [self packages]
  (db ["trying to give package" packages self.short-name])
  (if (. (lume.invert packages) self.short-name)
      (do
        (lume.remove packages self.short-name)
        (table.insert self.packages self.short-name)
        (db "success")
        (local pack (require :prefab-pack))
        (pack.set-delivered self.short-name)
        :give-pack)
    (do (db "failure") false)))

(fn characters.take-package [self packages]
  (db "trying to take package")
  (local name (get-package-name self))
  (if (and name (< (# packages) 4))
    (do (lume.remove self.packages name)
        (table.insert packages name)
        (db "success")
        :take-pack)
    (do (db "failure")
        false)))


(fn characters.has-package [self]
  (get-package-name self))

(fn characters.wants-package [self]
  (not (. (lume.invert self.packages) self.short-name)))

(fn characters.new [atlas name x y]
  (fn new-quad [member]
    (let [{: x : y : w : h} (. atlas.param.frames member :frame)]
      (love.graphics.newQuad  x y w h 128 512)))
  (local characters-quads [(new-quad (.. :characters- name :-talk-1))
                           (new-quad (.. :characters- name :-talk-2))
                           (new-quad (.. :characters- name :-idle-1))
                           (new-quad (.. :characters- name :-idle-2))
                           (new-quad (.. :characters- name :-wave-1))
                           (new-quad (.. :characters- name :-wave-2))
                           ])
  (local char-names [:k :jill :aly :sara
                     :tom :mike :carl :hank :post-office])
  (local name-map {:k 1 :jill 2 :aly 3 :sara 4
                   :tom 5 :mike 6 :carl 7 :hank 8 :p1 9
                   :p2 9 :p3 9 :p4 9 :p5 9 :p6 9 :p7 9 :p8 9})
  (local portrait-quads (icollect [_ n (ipairs char-names)]
                                   (new-quad (.. :portrait- n))))
  (local ex-quads [(new-quad :ex-1) (new-quad :ex-2)])
  (local question-quads [(new-quad :question-1) (new-quad :question-2)])
  (local happy-quads [(new-quad :happy-1) (new-quad :happy-2)])
  (local sad-quads [(new-quad :sad-1) (new-quad :sad-2)])
  (local shadow-quad (new-quad :shadow))
  (setmetatable {:pos {: x : y}
                 :image atlas.image
                 : characters-quads
                 : portrait-quads
                 : shadow-quad
                 : name-map
                 :packages []
                 :root {:x 0 :y -12}
                 :w 16 :h 32
                 :x x :y y ;; origional position
                 :action nil
                 :showing 0
                 :emotes (emote.new atlas -3 -20
                                   {: x : y}
                                   {:ex ex-quads :happy happy-quads
                                    :sad sad-quads :question question-quads})
                 :collision-type :cross
                 :animation {:frames [3 4] :timer 0 :index 1 :period 300 :flipped -1}
                 :col {:x -24 :y -12 :w 64 :h 32}
                 :type :characters
                 :short-name name
                 :name (.. :character "-" name)
                 :active true} characters-mt))

characters
