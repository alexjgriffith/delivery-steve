(local text {})

(fn text.collide [text])

(fn text.update [self dt])

(fn text.draw [self _ editor?]
  (love.graphics.setColor 1 1 1 1)
  (local quad self.text-quad)
  (if editor?
      :dont-draw
      (do
        (local params (require :params))
        (if self.in-sky
            (love.graphics.setColor params.colours.sky-text)
            (love.graphics.setColor params.colours.sky)
            )
        (if (. (require :state) :editor)
            (love.graphics.setColor 1 1 1 1)
            )
        (love.graphics.draw self.canvas self.pos.x self.pos.y))))

(local text-mt {:__index text
                :update text.update
                :collide text.collide
                :draw text.draw})

(fn text.update [self])

(fn text.init [self]
  (local world (. (require :state) :world))
  (local (items len) (world:queryRect
                      self.pos.x self.pos.y self.col.w self.col.h
                      #(and (. $1 :type)
                            (= (. $1 :type) :ground)
                            )))
  (when (> len 0)
    (tset self :in-sky false)
    )
  )

(local font (assets.fonts.fffforwa 16))
(font:setFilter :nearest :nearest)

(fn text.new [atlas level number x y]
  ;; (fn new-quad [member]
  ;;   (let [{: x : y : w : h} (. atlas.param.frames member :frame)]
  ;;     (love.graphics.newQuad  x y w h 512 512)))
  ;;(local text-quad (new-quad (.. :text- name)))
  (local notes (. (require :sky-notes)))
  (when (and (. notes level) (. notes level number) )
    (local text (. notes level number))
    (local width (+ (font:getWidth text.text) 16))
    (local canvas (love.graphics.newCanvas width 40))
    (canvas:setFilter :nearest :nearest)
    (love.graphics.push "all")
    (love.graphics.setCanvas canvas)
    (love.graphics.setColor 1 1 1 1)
    (love.graphics.setFont font)
    (love.graphics.print text.text 8 12)
    (love.graphics.pop)
    (local ret (setmetatable {:pos {: x : y}
                              :in-sky true
                              :root {:x 8 :y 32}
                              :x x :y y ;; origional position
                              :w 400 :h 80
                              : canvas
                              :collision-type :cross
                              :col {:x 0 :y 0 :w width :h 40}
                              :type :text
                              : number
                              : level
                              :name (.. :text- level "-" number)
                              :active true} text-mt))
    (when (. (require :state) :world)
      (ret:init)
      )
    ret

    ))

text
