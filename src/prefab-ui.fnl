(local ui {})
(local loader (require :lib.loader))



(fn ui.update [self dt]
  (local levels (require :levels))
  (local state (require :state))
  (local resources (. state :resources))
  ;; flag for global update
  (local delivered resources.delivered)
  (when (and (> self.max-packs 0) (not state.level.over) (= delivered self.max-packs))
    (love.event.push :transition state.current-level)
    (tset state.level :over true))
  (tset self :delivered delivered)
  (local steve (. (require :state) :steve))
  (when steve
    (tset self :packages steve.pacakges)
    (tset (. levels :list state.current-level) :score steve.score))
  (tset self :score 0)
  (each [_  {: score} (pairs (. levels :list))]
    (tset self :score (+ score self.score))
    ))

(local params (require :params))

(local ui-font (assets.fonts.fffforwa 12))

(fn ui.draw [self]
  (local current-level (. (require :state) :current-level))
  (local steve (. (require :state) :steve))
  (when (and steve.movement (~= current-level :opening)
             (~= current-level :post-game) (~= current-level :final-level))
    (love.graphics.setColor params.colours.ui-back)
    (local packages (. (require :state) :steve :packages))
    ;; (love.graphics.rectangle :fill 5 5 276 84 5)
    (love.graphics.rectangle :fill 5 5 (+ 276 125) 94 5)
    (love.graphics.setColor params.colours.ui-back-2)
    (love.graphics.rectangle :fill (+ 125 ) 10 (+ 276 ) 84 5)
    (love.graphics.setColor 1 1 1 1)
    (love.graphics.setFont ui-font)
    (local levels (require :levels))
    (local state (require :state))
    (local day (. levels :list state.current-level :display))
    (love.graphics.print day 15 (+ 5 5 5 5))
    (love.graphics.print (.. "Packs " self.delivered " / " self.max-packs) 15 (+ 5 5 5 30))
    (love.graphics.print (.. "Score " self.score)  15 (+  5 5 5 60 -5))
    (when (and packages (> (# packages) 0))
      (var i 0)
      (each [_ pack (ipairs packages)]
        (love.graphics.draw self.image
                            (. self.portrait-quads (. self.name-map pack))
                            (+ 15 125 (* i 64))
                             20
                             0
                             4
                             )
         (set i (+ 1 i))))))

(local ui-mt {:__index ui
                :update ui.update
                :draw ui.draw})

(fn ui.new []
  (local state (require :state))
  (var max-packs 0)
  (each [_ obj (pairs state.objects)]
    (when (and obj.type (= obj.type :pack))
      (set max-packs (+ 1 max-packs)))
    (when (and obj.type (= obj.type :characters)
               (not (obj:wants-package)))
      (set max-packs (- max-packs 1))
    ))
  (local atlas (loader.load-atlas "assets/data/sprite-data" "assets/sprites"))
  (fn new-quad [member]
    (let [{: x : y : w : h} (. atlas.param.frames member :frame)]
      (love.graphics.newQuad  x y w h 128 512)))
  (local char-names [:k :jill :aly :sara
                     :tom :mike :carl :hank :post-office])
  (local name-map {:k 1 :jill 2 :aly 3 :sara 4
                   :tom 5 :mike 6 :carl 7 :hank 8 :p1 9
                   :p2 9 :p3 9 :p4 9 :p5 9 :p6 9 :p7 9 :p8 9})
  (local portrait-quads (icollect [_ n (ipairs char-names)]
                                   (new-quad (.. :portrait- n))))
  (setmetatable {:image atlas.image
                 : name-map
                 : portrait-quads
                 : max-packs
                 :delivered 0
                 :score 0
                 :packages []
                 :type :ui
                 :active true} ui-mt))

ui
