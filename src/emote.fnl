(local emote {})

(local anim-timer {:timer 0
                   :period 0.4
                   :rises [1 2]
                   :index 1
                   :rcount 5
                   :r 1})

(fn update-timer [dt obj]
  (local {: period : rises} obj)
  (local next-time (+ dt obj.timer))
  (if (> next-time period)
      (do
        (tset obj :index (+ obj.index 1))
        (when (> obj.index (# rises))
          (tset obj :r (+ 1 obj.r))
          (tset obj :index 1))
        (tset obj :timer (- next-time period)))
      (tset obj :timer next-time)))

(fn get-rise [{: rises : index}] (. rises index))

(fn emote.update [self dt obj]
  (when self.active
    (tset self :pos obj.pos)
    (update-timer dt self.anim-timer)
    (when (and (not self.repeat) (>= self.anim-timer.r self.anim-timer.rcount))
      (tset self :active false))))

(fn emote.draw [self]
  (when self.active
    (love.graphics.draw self.image
                      (. self.quads self.active-emote (get-rise self.anim-timer))
                      (if (= -1 self.flipped)
                          (+ self.pos.x self.off.x 23)
                          (+ self.pos.x self.off.x))
                      (+ self.pos.y self.off.y)
                      0
                      self.flipped
                      1)))

(fn emote.set [self active-emote repeat]
  (when (or (not self.active)
             (~= self.active-emote active-emote))
    (tset self :active-emote active-emote)
    (tset self.anim-timer :timer 0)
    (tset self.anim-timer :index 1)
    (tset self.anim-timer :r 1)
    (tset self :repeat true)
    (tset self :active true)))

(fn emote.end [self]
  (tset self :active false))

(local emote-mt {:__index emote
                 :update emote.update
                 :draw emote.draw
                 :set emote.set
                 :end emote.end})

(fn emote.new [atlas x y pos quads]
  (setmetatable {:image atlas.image
                 : pos
                 :anim-timer (lume.clone anim-timer)
                 :off {: x : y}
                 : quads
                 :flipped 1
                 :repeat false
                 :active false
                 } emote-mt))

emote
