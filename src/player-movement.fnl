(local params (require :params))

(local rate 7)

(fn check-key-down [key]
  (local state (require :state))
  (if (-> state.keys
          (. key)
          (lume.map love.keyboard.isDown)
          (lume.reduce (fn [a b] (or a b))))
      1
      0))

(fn sign0 [x]
  (if (= x 0) 0
      (> x 0) 1
      (< x 0) -1))

(fn movement-fall [player dt]
  (local ratio (* 60 dt)) ;; game was developed while running 60fps
  (let [lr (- (check-key-down :right) (check-key-down :left))
        ud 0
        speed player.speed
        speed-params params.player-speed.fall
        pos player.pos
        max (if (and (~= 0 lr) (~= 0 ud)) speed-params.max-s2 speed-params.max)]
    (set speed.x (lume.clamp
                  (+ speed.x
                     (* dt (+ speed-params.decay speed-params.rate) lr)
                     (* dt (sign0 speed.x) (- speed-params.decay)))
                  (- max) max))
    (set speed.y (lume.clamp
                           (+ speed.y (* dt speed-params.gravity 1))
                           (- speed-params.fall-max) speed-params.fall-max))
    (when (and (< (math.abs speed.x) speed-params.floor) (= 0 lr) )
      (set speed.x 0))

    (local [goal-x goal-y] [(+ pos.x ( * rate (^ dt 1) speed.x))
                            (+ (* (^ dt 0.6)   speed.y) pos.y)])
    (local world (. (require :state) :world))
    (local (actual-x actual-y cols len)
           (world:move player (+ goal-x player.col.x)
                       (+ goal-y player.col.y)
                       (fn [item other]
                         (match other.type
                           :ground :slide
                           _ :cross))))
    (set pos.x (- actual-x player.col.x))
    (set pos.y (- actual-y player.col.y))
    ;; update-state-machine
    [cols len]))

(fn movement-walk [player dt]
  (local ratio (* 60 dt)) ;; game was developed while running 60fps
  (let [lr (- (check-key-down :right) (check-key-down :left))
        ud 0
        speed player.speed
        speed-params params.player-speed.walk
        pos player.pos
        max (if (and (~= 0 lr) (~= 0 ud)) speed-params.max-s2 speed-params.max)]
    (set speed.x (lume.clamp
                  (+ speed.x
                     (* dt (+ speed-params.decay speed-params.rate) lr)
                     (* dt (sign0 speed.x) (- speed-params.decay)))
                  (- max) max))
    (when (and (< (math.abs speed.x) speed-params.floor) (= 0 lr) )
      (set speed.x 0))
    (set speed.y 0)
    (local [goal-x goal-y] [(+ pos.x ( * rate (^ dt 1) speed.x))
                            (+ pos.y ( * ratio speed.y))])
    (local world (. (require :state) :world))
    (local (actual-x actual-y cols len)
           (world:move player (+ goal-x player.col.x)
                       (+ goal-y player.col.y)
                       (fn [item other]
                         (match other.type
                           :ground :slide
                           _ :cross))))
    (set pos.x (- actual-x player.col.x))
    (set pos.y (- actual-y player.col.y))
    [cols len]))

;; fix timestep 0.1s
;; y position updates at 20fps
(local period 0.03)
(var timestep 0)
(fn movement-jump [player dt]
  (local ratio (/ 1 (* 60 dt))) ;; game was developed while running 60fps
  (let [lr (- (check-key-down :right) (check-key-down :left))
        ud 0
        speed player.speed
        speed-params params.player-speed.jump
        pos player.pos
        max  speed-params.max-s2
        jump-max speed-params.jump-max]
    (set speed.x (lume.clamp
                  (+ speed.x
                     (* dt ratio (+ speed-params.decay speed-params.rate) lr)
                     (* dt ratio (sign0 speed.x) (- speed-params.decay)))
                  (- max) max))
    ;; (local speedy2 (lume.clamp
    ;;                 (+ speed.y
    ;;                    (* dt ratio speed-params.gravity))
    ;;                 (- jump-max) 0))
    ;;(local speedy2 (+ speed.y (* dt speed-params.gravity)))
    ;;(local posy (+ (* speed.y dt) (* 0.5 dt dt speed-params.gravity) ))
    ;; (set speed.y speed.y)
    (set timestep (+ timestep dt))
    (var posy pos.y)
    (when (> timestep period)
      ;; gloal = 1 in 2 s from jump initiation
      ;; (tset speed :y (- (* 0.5 player.controler.jump.timer.time 10) 8))
      (set speed.y (+ speed.y (* (* period speed-params.gravity) )))
      (set timestep (- timestep period))
      ;;(set posy (+ speed.y pos.y))
      )
    (when (and (< (math.abs speed.x) speed-params.floor) (= 0 lr) )
      (set speed.x 0))
    (when (and (< (math.abs speed.y) speed-params.floor) (= 0 lr) )
      (set speed.y 0))
    (local [goal-x goal-y] [(+ pos.x  (* rate (^ dt 1) speed.x))
                            (+ (* (^ dt 0.6)   speed.y) pos.y)])
    ;;(tset speed :y speedy2)
    (local world (. (require :state) :world))
    (local (actual-x actual-y cols len)
           (world:move player (+ goal-x player.col.x)
                       (+ goal-y player.col.y)
                       (fn [item other]
                         (match other.type
                           :ground :slide
                           _ :cross))))
    (set pos.x (- actual-x player.col.x))
    (set pos.y (- actual-y player.col.y))
    [cols len]))



{ : movement-walk : movement-fall : movement-jump}
