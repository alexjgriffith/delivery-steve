(local portraits {})

(fn portraits.collide [portraits])

(fn portraits.update [self dt])

(fn portraits.draw [self]
  (love.graphics.setColor 1 1 1 1)
  (local quad self.portrait-quad)
  (love.graphics.draw self.image quad self.pos.x self.pos.y))

(local portraits-mt {:__index portraits
                :update portraits.update
                :collide portraits.collide
                :draw portraits.draw})

(fn portraits.new [atlas name x y]
  (fn new-quad [member]
    (let [{: x : y : w : h} (. atlas.param.frames member :frame)]
      (love.graphics.newQuad  x y w h 128 512)))
  (local portrait-quad (new-quad (.. :portrait- name )))
  (setmetatable {:pos {: x : y}
                 :image atlas.image
                 : portrait-quad
                 :root {:x 8 :y 32}
                 :w 32 :h 32
                 :x x :y y ;; origional position
                 :collision-type :cross
                 :col {:x 0 :y 0 :w 32 :h 32}
                 :type :portraits
                 : name
                 :active true} portraits-mt))

portraits
