(local overlay {})

(local button-font (assets.fonts.fffforwa 20))
(button-font:setFilter :nearest :nearest)
(local title-font (assets.fonts.fffforwa 80))
(title-font:setFilter :nearest :nearest)

(fn make-button [name]
  (local loader (require :lib.loader))
  (local house-atlas (loader.load-atlas "assets/data/house-data" "assets/sprites"))
  (fn new-quad [member]
    (let [{: x : y : w : h} (. house-atlas.param.frames member :frame)]
      (love.graphics.newQuad  x y w h 512 512)))
  (let [quad (new-quad name)
        image house-atlas.image]
    (fn [self]
      (local params (require :params))
      (love.graphics.setColor params.colours.ui-back)

      (if self.hovered
          (do (love.graphics.setColor params.colours.ui-back)
              (love.graphics.rectangle :fill self.box.x self.box.y self.box.w self.box.h 4)
              (love.graphics.setColor params.colours.ui-text))
          (do (love.graphics.setColor params.colours.ui-back-2)
              (love.graphics.rectangle :fill self.box.x self.box.y self.box.w self.box.h 4)
              (love.graphics.setColor params.colours.sky)))
      (love.graphics.draw image quad self.box.x self.box.y 0 self.scale))))

(fn make-text-button [text]
  (fn [self]
    (local params (require :params))
    (love.graphics.setFont button-font)
    (if self.hovered
        (do (love.graphics.setColor params.colours.ui-back-2)
            (love.graphics.rectangle :fill self.box.x self.box.y self.box.w self.box.h 4)
            (love.graphics.setColor params.colours.ui-text)
            )
        (love.graphics.setColor params.colours.sky-text))
    (love.graphics.rectangle :line self.box.x self.box.y self.box.w self.box.h 4)
    (love.graphics.printf text self.box.x (+ self.box.y 20) self.box.w :center)))

(fn make-title [text]
  (fn [self]
    (local params (require :params))
    (love.graphics.setFont title-font)
    (love.graphics.setColor params.colours.sky-text)
    (love.graphics.printf text self.box.x self.box.y self.box.w :center)))

(fn make-signature [text]
  (fn [self]
    (local params (require :params))
    (love.graphics.setFont button-font)
    (love.graphics.setColor params.colours.sky-text)
    (love.graphics.printf text self.box.x self.box.y self.box.w :center)))


(fn make-score []
  (fn [self]
    (local params (require :params))
    (local score (. (require :state) :ui :score))
    (love.graphics.setFont button-font)
    (love.graphics.setColor params.colours.sky-text)
    (love.graphics.printf (.. "Final Score: " score) self.box.x self.box.y self.box.w :center)))

(local levels (require :levels))

(fn load-level [level]
  (fn []
    (local state (require :state))
    (assets.sounds.click:setVolume 0.4)
    (assets.sounds.click:play)
    (tset state.level :over true)
    (var next-level :turorial-1)
    (each [key value (pairs levels.list)]
      (when (= value.next-level level)
      (tset state :current-level key)))
    (love.event.push :transition level)
    (tset state :menu false)

  ;; (state.steve:unlock-movement)
  ))


(fn toggle-menu []
  (assets.sounds.click:setVolume 0.4)
  (assets.sounds.click:play)
  (local state (require :state))
  (local steve (. state :steve))
  (if state.menu
      (do (tset state :menu false)
          (steve:unlock-movement))
      (do (tset state :menu true)
          (steve:lock-movement))
      ))

(tset overlay :buttons
      {:tutorial-1 {:name :tutorial-1
                    :scale 1
                    :box {:x (- 640 320) :y 50 :w 200 :h 64}
                    :draw (make-text-button "Day 1")
                    :active false
                    :action (load-level :tutorial-1)}
       :tutorial-2 {:name :tutorial-2
                    :scale 1
                    :box {:x (- 640 320) :y (+ 64 10 50) :w 200 :h 64}
                    :draw (make-text-button "Day 2")
                    :active false
                    :action (load-level :tutorial-2)
                    }
       :tutorial-3 {:name :tutorial-2
                    :scale 1
                    :box {:x (- 640 320) :y (+ 64 64 20 50) :w 200 :h 64}
                    :draw (make-text-button "Day 3")
                    :active false
                    :action (load-level :tutorial-3)
                    }
       :level-1 {:name :level-1
                    :scale 1
                    :box {:x (- 640 100) :y 50 :w 200 :h 64}
                    :draw (make-text-button "Day 4")
                    :active false
                    :action (load-level :level-1)}
       :level-2 {:name :level-2
                    :scale 1
                    :box {:x (- 640 100) :y (+ 64 10 50) :w 200 :h 64}
                    :draw (make-text-button "Day 5")
                    :active false
                    :action (load-level :level-2)
                    }
       :level-3 {:name :level-3
                    :scale 1
                    :box {:x (- 640 100) :y (+ 64 64 20 50) :w 200 :h 64}
                    :draw (make-text-button "Day 6")
                    :active false
                    :action (load-level :level-3)
                 }
       :level-4 {:name :level-4
                    :scale 1
                    :box {:x (+ 640 120) :y 50 :w 200 :h 64}
                    :draw (make-text-button "Day 7")
                    :active false
                    :action (load-level :level-4)}
       :level-5 {:name :level-5
                    :scale 1
                    :box {:x (+ 640 120) :y (+ 64 10 50) :w 200 :h 64}
                    :draw (make-text-button "Day 8")
                    :active false
                    :action (load-level :level-5)
                    }
       :level-6 {:name :level-6
                    :scale 1
                    :box {:x (+ 640 120) :y (+ 64 64 20 50) :w 200 :h 64}
                    :draw (make-text-button "Day 9")
                    :active false
                    :action (load-level :level-6)
                 }
       :back {:name :back
                    :scale 1
                    :box {:x (- 640 215) :y (+ 64 64 64 30 50) :w 200 :h 64}
                    :draw (make-text-button "Back")
                    :active false
                    :action toggle-menu
              }
       :quit {:name :quit
                    :scale 1
                    :box {:x (+ 640 15) :y (+ 64 65 64 30 50) :w 200 :h 64}
                    :draw (make-text-button "Quit")
                    :active false
              :action (fn [] (local state (require :state))
                        (when (not state.web) (love.event.quit)))
                    }
       :reset {:name :reset
                  :scale 2
                  :box {:x (- 1280 32 32 32 32 5 2 2 2) :y 5 :w 32 :h 32}
                  :draw (make-button :reset)
                  :active true
                :action (fn [] (levels.revert (require :state))
                          (local state (require :state))
                          (assets.sounds.click:play)
                          (state.steve:unlock-movement)
                          )}
       :full {:name :full
               :scale 2
               :active true
               :box {:x (- 1280 32 32 32 5 2 2) :y 5 :w 32 :h 32}
               :draw (make-button :full)
              :action (fn []
                        (assets.sounds.click:play)
                        (toggle-fullscreen))}
       :mute {:name :mute
               :scale 2
               :active true
               :box {:x (- 1280 32 32 5 2) :y 5 :w 32 :h 32}
               :draw (make-button :mute)
               :action (fn [] (set-mute true))}
        :unmute {:name :unmute
                 :scale 2
                 :active false
                 :box {:x (- 1280 32 32 5 2) :y 5 :w 32 :h 32}
                 :draw (make-button :unmute)
                 :action (fn []
                           (assets.sounds.click:play)
                           (set-mute false))}
        :menu {:name :menu
               :scale 2
               :active true
               :box {:x (- 1280 32 5 ) :y 5 :w 32 :h 32}
               :draw (make-button :menu)
               :action toggle-menu}
        :play {:name :play
               :scale 1
               :active true
               :box {:x 500 :y 584 :w 280 :h 64}
               :draw (make-text-button :Play)
               :action (fn []
                         (when assets
                           (local state (require :state))
                           (tset state.level :over true)
                           (assets.sounds.click:setVolume 0.4)
                           (assets.sounds.click:play)
                           (love.event.push :transition :opening))
                         )}
        :play-again {:name :play-again
               :scale 1
               :active true
               :box {:x 500 :y 640 :w 280 :h 64}
               :draw (make-text-button "Play Again")
               :action (fn []
                         (when assets
                           (local state (require :state))
                           (tset state.level :over true)
                           (assets.sounds.click:setVolume 0.4)
                           (assets.sounds.click:play)
                           (love.event.push :transition :opening))
                         )}
       :credits {:name :credits
                 :scale 1
                 :active true
                 :box {:x 500 :y 584 :w 280 :h 64}
                 :draw (make-text-button :Credits)
                 :action (fn []
                           (when assets
                             (local state (require :state))
                             (tset state.level :over true)
                             (assets.sounds.click:setVolume 0.4)
                             (assets.sounds.click:play)
                             (love.event.push :transition :final-level))
                           )}
       :title {:name :title
                :scale 1
                :active true
                :box {:x 100 :y 200 :w 1100 :h 32}
                :draw (make-title "Delivery Steve")
               }
       :signature {:name :signature
                :scale 1
                :active true
                :box {:x 450 :y 680 :w 1100 :h 32}
                :draw (make-signature "AlexJGriffith")
                   }
       :score {:name :score
                :scale 1
                :active true
                :box {:x 0 :y 520 :w 1280 :h 32}
                :draw (make-score)
                }
        })

(fn point-in-box [x y box]
  (and (> x box.x) (< x (+ box.x box.w))
       (> y box.y) (< y (+ box.y box.h))))

(fn overlay.update [self dt]
  (if (is-mute)
      (do (tset overlay.buttons.mute :active false)
          (tset overlay.buttons.unmute :active true))
      (do (tset overlay.buttons.mute :active true)
          (tset overlay.buttons.unmute :active false)))
  (local state (require :state))
  (local steve (. state :steve))
  (if steve.movement
      (tset overlay.buttons.reset :active true)
      (tset overlay.buttons.reset :active false)
      )
  (local current-level (. state :current-level))

  (tset overlay.buttons.play-again :active false)
  (tset overlay.buttons.play :active false)
  (tset overlay.buttons.credits :active false)
  (tset overlay.buttons.title :active false)
  (tset overlay.buttons.signature :active false)
  (tset overlay.buttons.score :active false)
  (match current-level
    :opening (do
               (tset overlay.buttons.title :active true)
               (tset overlay.buttons.play :active true)
               (tset overlay.buttons.signature :active true)
               )
    :final-level (do
                   (tset overlay.buttons.credits :active true)
                   (tset overlay.buttons.score :active true)
                   )
    :post-game (do
                   (tset overlay.buttons.play-again :active true)
                   )
    )

  (if state.menu
      (do
        (tset overlay.buttons.title :active false)
        (tset overlay.buttons.play :active false)
        (tset overlay.buttons.credits :active false)
        (tset overlay.buttons.tutorial-1 :active true)
        (tset overlay.buttons.tutorial-2 :active true)
        (tset overlay.buttons.tutorial-3 :active true)
        (tset overlay.buttons.level-1 :active true)
        (tset overlay.buttons.level-2 :active true)
        (tset overlay.buttons.level-3 :active true)
        (tset overlay.buttons.level-4 :active true)
        (tset overlay.buttons.level-5 :active true)
        (tset overlay.buttons.level-6 :active true)
        (tset overlay.buttons.back :active true)
        (tset overlay.buttons.quit :active true)
        )
      (do
        (tset overlay.buttons.tutorial-1 :active false)
        (tset overlay.buttons.tutorial-2 :active false)
        (tset overlay.buttons.tutorial-3 :active false)
        (tset overlay.buttons.level-1 :active false)
        (tset overlay.buttons.level-2 :active false)
        (tset overlay.buttons.level-3 :active false)
        (tset overlay.buttons.level-4 :active false)
        (tset overlay.buttons.level-5 :active false)
        (tset overlay.buttons.level-6 :active false)
        (tset overlay.buttons.quit :active false)
        (tset overlay.buttons.back :active false)
        )
      )

  (local (ix iy) (love.mouse.getPosition))
  (local [x y] [ (/ (- ix offsets.gx) offsets.s) (/ (- iy offsets.gy) offsets.s)])
  (each [_ button (pairs self.buttons)]
    (if (and  button.box (point-in-box x y button.box))
        (tset button :hovered true)
        (tset button :hovered false))))

(fn overlay.mousepressed [self x y button]
  (var clicked false)
  (each [_ button (pairs self.buttons)]
    (when (and button.action button.active (point-in-box x y button.box))
      (set clicked true)))
  clicked)

(fn overlay.mousereleased [self x y button]
  (var clicked false)
  (each [_ button (pairs self.buttons)]
    (when (and button.action button.active (point-in-box x y button.box))
      (button.action)
      (set clicked true)))
  clicked)

(fn overlay.draw [self]
  (love.graphics.push "all")
  (love.graphics.setColor 1 1 1 1)
  (each [_ button (pairs self.buttons)]
    (when button.active
      (button:draw)))
  (love.graphics.pop))

(fn overlay.escape []
  (toggle-menu))

(fn overlay.enter []
  (when overlay.buttons.play.active
    (overlay.buttons.play.action)))

(fn overlay.load-level [level]
  ((load-level level)))

overlay
