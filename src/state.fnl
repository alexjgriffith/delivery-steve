{:current-level "opening"
 :editor false
 :menu false
 :dev false
 :web false
 :colliders false
 :fast-render true
 :fullscreen false
 :camera-type :lag-lead
 :fps false
 :keys {:left ["left" "a"]
        :right ["right" "d"]
        :jump ["up" "w"]
        :action ["space"]}}
