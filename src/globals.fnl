(global offsets {:x 0 :y 0 :s 1 :sx 1 :sy 1 :gx 0 :gy 0})

(global screenshot
        (fn []
          (_G.assets.sounds.click:play)
          (love.graphics.captureScreenshot
           (string.format "screenshot-%s.png" (os.time)))))

(var mute false)
(global toggle-sound
        (fn  []
          (if mute
              (do
                (_G.assets.sounds.click:play)
                (do (love.audio.setVolume 1) (set mute false)))
              (do  (love.audio.setVolume 0) (set mute true)))))

(global is-mute (fn [] mute))

(global set-mute (fn [in-mute]
                   (if in-mute
                         (do (love.audio.setVolume 0) (set mute true))
                         (do  (love.audio.setVolume 1) (set mute false)))))


(global resize (fn []
  (local (x y flags) (love.window.getMode))
  (local sx (/ x 1280))
  (local sy (/ y 720))
  (tset offsets :sx sx)
  (tset offsets :sy sy)
  (tset offsets :s (math.min sx sy))
  (tset offsets :x (math.max 0 (-  (/ x 2) (/ flags.minwidth 2))))
  (tset offsets :y (math.max 0 (-  (/ y 2) (/ flags.minheight 2))))
  (if (< sx sy)
      (do (tset offsets :gx 0)
          (tset offsets :gy (/ (- y (* offsets.s 720)) 2)))
      (do
        (tset offsets :gx (/ (- x (* offsets.s 1280)) 2))
        (tset offsets :gy 0)))))

(fn _toggle-fullscreen []
  (local state (require :state))
  (local (pw ph flags) (love.window.getMode))
  (local [w h] [flags.minwidth flags.minheight])
  (local (dx dy) (love.window.getDesktopDimensions flags.display))
  (if flags.fullscreen
      (do
        (tset state :fullscreen false)
        (tset flags :fullscreen false)
        (tset flags :centered true)
        (tset flags :x (math.max 0 (/ (- dx 1280) 2)))
        (tset flags :y (math.max 0 (/ (- dy 720) 2)))
        (love.window.setMode w h flags)
        (love.mouse.setGrabbed false))

      (do
        (love.window.setFullscreen true :desktop)
        (tset flags :fullscreen true)
        (tset state :fullscreen true)
        (love.window.setMode w h flags)
        (love.mouse.setGrabbed true)
        ;; (love.mouse.setGrabbed true)
        ))
  ;; (tset flags :centered true)

  (local state (require :state))
  (when (and state state.snow)
    (tset state :snow (state.snow.new 10 10 20 100))) ;; bugs out when fs toggled)
  (resize)
  (local (cw ch _flags) (love.window.getMode))
  (tset offsets :x (math.max 0 (math.floor (-  (/ cw 2) (/ w 2)))))
  (tset offsets :y (math.max 0 (math.floor (-  (/ ch 2) (/ h 2))))))

(global is-fullscreen
        (fn []
          (local (_w _h flags) (love.window.getMode))
          flags.fullscreen))

(global toggle-fullscreen
        (fn []
          (local state (require :state))
          (if state.web (JS.callJS "toggleFullScreen();")
              (_toggle-fullscreen))))
