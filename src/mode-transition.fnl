(local transition {})

(var timer 0)

(local state (require :state))

(var step :leave)

(var alpha 1)

(local gamestate (require :lib.gamestate))

(local params (require :params))

(local mode-game (require :mode-game))

(local levels (require :levels))

(fn transition-colour [a] [(/ 67 256) (/ 52 256) (/ 85 256) a])

(local title-font (assets.fonts.fffforwa 100))

(fn transition.draw [self]
  (mode-game.draw self)
  (love.graphics.push "all")
  (love.graphics.translate offsets.gx offsets.gy)
  (love.graphics.scale offsets.s)
  (when (~= 0 alpha)
    (love.graphics.setColor (transition-colour (lume.clamp alpha 0 1)))
    (love.graphics.rectangle "fill" 0 0 1280 780))
  (love.graphics.setColor 1 1 1 1)
  ;; (love.graphics.rectangle "line" 1 1 1278 718)
  (when (= step :load)
    (love.graphics.setFont title-font)
    (local state (require :state))
    (local level (. (require :levels) :list state.current-level :display))
    (love.graphics.printf level 0 300 1280 :center))
  (when (= step :done) (gamestate.switch (require :mode-game) :resume))
  (love.graphics.pop))


(fn transition.update [self dt]
  ;; (mode-game.update self dt)
  (mode-game.update self dt)
  (match step
    :leave
    (do
      (if (> timer 1)
          (do
            (set step :load)
            (set timer 0)
            (set alpha 1)
            (levels.next state)
            (: (. (require :state) :steve) :lock-movement)
            )
          (do
            (set timer (+ timer dt))
            (set alpha timer)))
      )
    :load (if (> timer 3)
              (let [current-level (. (require :state) :current-level)]
                (when (not (or (= current-level :opening)
                               (= current-level :final-level)
                               ))
                  (: (. (require :state) :steve) :unlock-movement))
                (set step :enter)
                (set timer 0))
              (do
                (set timer (+ timer dt))))
    :enter
    (do
      (if (< alpha 0)
          (do
            (set timer 0)
            (set step :done)
            (set alpha 0)
            )
          (do (set timer (+ timer dt))
              (set alpha (/ (- 2 timer) 2)))
      ))
    )
  )

(fn transition.enter [self from ...]
  ;; (love.mouse.setVisible false)
  (set step :leave)
  (when (not (. levels :list state.current-level :next-level))
    (love.event.quit)
    ;;(gamestate.switch (require :mode-end-state))
    ))

(fn transition.leave [self from ...]
)

(fn transition.mousereleased [self ...])

(fn transition.keypressed [self key code]
  (match key
    :space (state.steve:action)
    :f10 (toggle-fullscreen)
    :q   (when (not state.web) (screenshot))
    ))

transition
