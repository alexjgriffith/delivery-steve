(local floaty {})

(tset floaty :active [])

(fn floaty.draw []
  (local float-font (assets.fonts.fffforwa (* 30 offsets.s)))
  (float-font:setFilter :nearest :nearest)

  (local {:x ix :y iy} (. (require :state) :steve :pos))
  (local camera (. (require :state) :camera))
  ;; (local i   (- (/ x camera.scale) camera.x))
  ;; (local j   (- (/ y camera.scale) camera.y))

  (local i (* (+ ix camera.x) camera.scale))
  (local j (* (+ iy camera.y) camera.scale))

  (local [x y] [(* (+ i offsets.gx) offsets.s)
                 (* (+ j offsets.gy) offsets.s)])

  (love.graphics.push "all")
  (love.graphics.setFont float-font)
  (each [_ float (ipairs floaty.active)]
    (love.graphics.setColor (float.colour float.alpha))
    (love.graphics.printf float.value (+ x 10) (+ y -30 float.oy) 70 :center))
  (love.graphics.pop))

(fn floaty.update [dt]
  (each [_ float (ipairs floaty.active)]
    (set float.time (+ float.time dt))
    (set float.alpha (- float.period float.time))
    (set float.oy (- float.oy (* dt float.rate))))
  )

(fn floaty.new [value]
  (when (~= 0 value)
    (table.insert floaty.active
                  {: value
                   :colour (if (< value 0)
                               (fn [alpha] [(/ 246 256) (/ 117 256) (/ 122 256) alpha])
                               (fn [alpha] [(/ 99 256) (/ 199 256) (/ 77 256) alpha]))
                   :time 0
                   :period 2
                   :oy 0
                   :alpha 1
                   :rate 200
                   }))
  )



floaty
