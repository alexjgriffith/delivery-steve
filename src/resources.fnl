(fn []
  (local state (require :state))
 (local resources (. state :resources))

  (var delivered 0)
  (each [_ obj (pairs state.objects)]

    ;; Get the number of delivered packages
    (when (and obj.type (= obj.type :pack) obj.delivered)
      (set delivered (+ 1 delivered))))


  (tset resources :delivered delivered)
  )
