(local pack {})

(fn pack.collide [pack])

(fn pickup [self]
  (fn [player]
    (if (and (not self.held?)
             (< (# player.packages) 4))
        (do

          (table.insert player.packages self.number)
          (tset self :held? true)
          :pickup-pack)
        false)
    ))



(fn pack.update [self dt]
  (set self.time (+ self.time dt))
  (when (> self.time self.period)
    (set self.time 0)
    (set self.index (+ self.index 1)))
  (when (> self.index 2)
    (set self.index 1))
  (if self.held? (tset self :action nil)
      (not self.action) (tset self :action (pickup self))))

(local number-font ((. assets.fonts "inconsolata") 20))

(fn pack.draw [self _ editor?]
  (love.graphics.setColor 1 1 1 1)
  (local quad (. self.pack-quad self.index))
  (if editor?
      (do
        (love.graphics.push "all")
        (love.graphics.setFont number-font)
        (love.graphics.setColor (/ 181 256) (/ 80 256) (/ 136 256))
        ;;(love.graphics.rectangle :fill 0 0 8 8)
        (love.graphics.setColor 1 1 1 1)
        (love.graphics.draw self.image quad -5 -20 0 4)
        (love.graphics.setColor 1 1 1 1)
        (love.graphics.printf self.number 0 17 50 :center)
        (love.graphics.pop)
        )
      (not self.held?)
      (do
        (local state (require :state))
        (local steve state.steve)
        (local d1 (lume.distance (+ steve.pos.x 8) steve.pos.y self.pos.x self.pos.y))
        ;; Draw Portrait
        (when (or (< d1 32) state.editor)
            (love.graphics.draw self.image self.portrait-quad
                            (+ self.root.x self.pos.x)
                            (+ self.root.y self.pos.y -34)
                            0
                            1))
        ;; Draw Package
        (love.graphics.draw self.image quad (+ self.root.x self.pos.x)
                          (+ self.root.y self.pos.y)))))

(local pack-mt {:__index pack
                :update pack.update
                :collide pack.collide
                :draw pack.draw})

(fn pack.place [self x y]
  (tset self :held? false)
  (tset self.pos :x (+ x 8))
  (tset self.pos :y (+ y 26))
  (local state (require :state))
  (state.world:update self (- self.pos.x 8) (- self.pos.y 8))
  )

(fn pack.set-delivered [name]
  (local state (require :state))
  ;; (each [_ obj (pairs state.objects)]
  ;;   (when (and (= obj.type :pack) (= obj.number name))
  ;;     (tset obj :delivered true))))
  (let [obj (. state :objects (.. :package- name))]
    (when obj
      (tset obj :delivered true))))

(fn pack.new [atlas number x y held?]
  (fn new-quad [member]
    (let [{: x : y : w : h} (. atlas.param.frames member :frame)]
      (love.graphics.newQuad  x y w h 128 512)))
  (local pack-quad [(new-quad (.. :pack-ground-1))
                    (new-quad (.. :pack-ground-2))])

  (local highlight-quad [(new-quad (.. :pack-ground-3))
                         (new-quad (.. :pack-ground-4))])

  (local char-names [:k :jill :aly :sara
                     :tom :mike :carl :hank :post-office])
  (local name-map {:k 1 :jill 2 :aly 3 :sara 4
                   :tom 5 :mike 6 :carl 7 :hank 8 :p1 9
                   :p2 9 :p3 9 :p4 9 :p5 9 :p6 9 :p7 9 :p8 9})
  (local portrait-quads (icollect [_ n (ipairs char-names)]
                                   (new-quad (.. :portrait- n))))
  (local portrait-quad (. portrait-quads (. name-map number)))
  (setmetatable {:pos {: x : y}
                 :image atlas.image
                 : pack-quad
                 : highlight-quad
                 : portrait-quad
                 : name-map
                 :x x :y y ;; origional position
                 :w 16 :h 16
                 :collision-type :cross
                 :col {:x -8 :y -8 :w 32 :h 16}
                 :root {:x 0 :y -8}
                 :type :pack
                 : held?
                 : number
                 :time (lume.random 0 6)
                 :index (math.floor (lume.random 1 2))
                 :period (/ 90 60)
                 :action nil
                 :name (.. :package- number)
                 :active true} pack-mt))

pack
