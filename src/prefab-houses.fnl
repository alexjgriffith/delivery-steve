(local houses {})

(fn houses.collide [houses])

(local timer (require :lib.timer))


(fn houses.update [self dt]
  (tset self.animation :timer (+ dt self.animation.timer))
  (when (> self.animation.timer self.animation.period)
    (tset self.animation :timer 0)
    (tset self.animation :index (+ self.animation.index 1)))
  (when (> self.animation.index (# self.animation.frames))
    (tset self.animation :index 1))
  (self.smoke:update dt))

(fn houses.draw [self _ editor?]
  (local current-level (. (require :state) :current-level))
  (love.graphics.setColor 1 1 1 1)
  (local quad self.houses-quad)
  (local {: index : frames} self.animation)
  (local oy (. frames index))
  (local name-quad self.name-quad)
  (if (= editor? :editor)
    (love.graphics.draw self.image quad 2 2 0 0.5)
    (do
      (when (~= :final-level current-level)
       (self.smoke:draw)
       (love.graphics.draw self.image name-quad (- self.pos.x 40) (- self.pos.y 30 (- oy))))
      (love.graphics.draw self.image quad (+ self.root.x self.pos.x)
                          (+ self.root.y self.pos.y))

      )))

(local houses-mt {:__index houses
                :update houses.update
                :collide houses.collide
                :draw houses.draw})

(fn houses.new [atlas name x y chim]
  (local [chim-x chim-y] chim)
  (local smoke-module (require :smoke))
  (fn new-quad [member]
    (let [{: x : y : w : h} (. atlas.param.frames member :frame)]
      (love.graphics.newQuad  x y w h 512 512)))
  (local houses-quad (new-quad (.. :house- name )))
  (local name-quad (new-quad (.. :text-house- name )))
  (local smoke (smoke-module.new atlas :particle-c-3 {:x (+ x chim-x) :y (+ y chim-y)}))
  (setmetatable {:pos {: x : y}
                 : smoke
                 :image atlas.image
                 : houses-quad
                 : name-quad
                 :root {:x 8 :y 32}
                 :x x :y y ;; origional position
                 :w 80 :h 80
                 :collision-type :cross
                 :col {:x -20 :y -30 :w 120 :h 110}
                 :root {:x 0 :y +3}
                 :type :houses
                 :animation {:timer (lume.random 0.1 0.29) :index (math.floor (lume.random 2 7)) :frames [2 2 2 2 2 2 2 2 1 1 1] :period 0.3}
                 :name (.. :house- name)
                 :active true} houses-mt))

houses
