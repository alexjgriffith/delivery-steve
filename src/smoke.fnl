(local smoke {})

(fn smoke.update [self dt]
  (local part self.part)
  (self.part:update dt))

(fn smoke.draw [self]
  (love.graphics.setColor 1 1 1 1)
  (love.graphics.draw self.part self.pos.x self.pos.y)
  ;; (love.graphics.rectangle :fill (- self.pos.x 1) (- self.pos.y 1) 2 2)
  )

(fn setup-smoke [canvas]
  (local part (love.graphics.newParticleSystem canvas 100))
  (part:setEmissionRate 5)
  (part:setSizeVariation 1)
  (part:setSpeed 2 2 )
  (part:setRotation -1 1)
  (part:setSpin 1)
  (part:setParticleLifetime 3 7)
  (part:setLinearAcceleration -10 -50 10 0)
  (part:setSpread (/ math.pi 2))
  (part:setLinearDamping 10)
  (part:setColors 1 1 1 1 1 1 1 0)
  part)


(fn setup-grass [canvas]
  (local part (love.graphics.newParticleSystem canvas 100))
  part)

(local smoke-mt {:__index smoke
                 :update smoke.update
                 :draw smoke.draw
                 :new smoke.new})

(fn smoke.new [atlas particle pos which?]
  (fn new-quad [member]
    (let [{: x : y : w : h} (. atlas.param.frames member :frame)]
      (love.graphics.newQuad  x y w h 512 512)))
  (local canvas (love.graphics.newCanvas 4 4))
  (local quad (new-quad particle))
  (love.graphics.push "all")
  (love.graphics.setCanvas canvas)
  (love.graphics.draw atlas.image quad)
  (love.graphics.pop)
  (local setup (. {:smoke setup-smoke :grass setup-grass} (or which? :smoke)))
  (local part (setup canvas))
  (setmetatable { : canvas : part : pos} smoke-mt))

smoke
