(local particles {})

(fn new-particle [particle]
  (local smoke (require :smoke))
  (local loader (require :lib.loader))
  (local atlas (loader.load-atlas "assets/data/house-data" "assets/sprites"))
  (fn new-quad [member]
    (let [{: x : y : w : h} (. atlas.param.frames member :frame)]
      (love.graphics.newQuad  x y w h 512 512)))
  (local canvas (love.graphics.newCanvas 4 4))
  (local quad (new-quad particle))
  (love.graphics.push "all")
  (love.graphics.setCanvas canvas)
  (love.graphics.draw atlas.image quad)
  (love.graphics.pop)
  (love.graphics.newParticleSystem canvas 100)
  )

(fn setup-burst [self]
  (local obj self.obj)
  (obj:setSizeVariation 0)
  (obj:setRotation 0)
  (obj:setSpin 1)
  (obj:setParticleLifetime 0.05 0.2 )
  (obj:setSizes 0 1 0)
  (obj:setLinearAcceleration -4000 -4000 4000 0)
  ;; (obj:setLinearAcceleration 0 0 0 0)
  (obj:setSpread 0.5)
  (obj:setLinearDamping 30 50)
  (obj:setColors 1 1 1 1 1 1 1 0)
  )

(tset particles :list
      {:land
       {:time 0
        :max 0.1
        :off 3
        :setup setup-burst
        :particle :particle-c-3
        :obj (new-particle :particle-c-1)
        :pos {:x 0 :y 0}
        }
       }
      )

(tset particles :active
      [])

(fn particles.new [name x y]
  (local part (lume.clone (. particles :list name)))
  (tset part :obj (new-particle part.particle))
  (tset part :time 0)
  (part:setup)
  (tset part :pos {: x : y})
  (table.insert particles.active part)
  )

(fn particles.update [self dt]
  (local remove [])
  (each [key part (pairs particles.active)]
    (local obj part.obj)
    ;; (obj:setPosition part.pos.x part.pos.y)
    (if (< part.time part.max)
        (obj:setEmissionRate 20)
        (obj:setEmissionRate 0))
    (part.obj:update dt)
    (set part.time (+ part.time dt))
    (when (> part.time part.off)
      (part.obj:release)
      (table.insert remove key)))
  (each [_ rem (ipairs remove)]
    (table.remove particles.active rem)))

(fn particles.draw []
  (each [_ part (pairs particles.active)]
    ;; (love.graphics.rectangle :fill part.pos.x part.pos.y 3 3)
    (love.graphics.draw part.obj  part.pos.x part.pos.y 0 1)))

particles
