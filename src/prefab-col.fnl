(local col {})

(local bump (require :lib.bump))

(fn col.draw [world]
  (love.graphics.push "all")
  (local (items len) (world:getItems))
  (love.graphics.setColor 1 0 0 1)
  (each [key value (pairs items)]
    (when (or (= :player value.type) (= :ground value.type)
              (= :houses value.type) (= :pack value.type)
              (= :post-office value.type) (= :characters value.type)
              (= :text value.type) (= :wood value.type)
              )
      (local (x y w h) (world:getRect value))
      (love.graphics.rectangle "line" x y w h)))
  (love.graphics.pop))


;; add tile
(fn col.add-tile [world mapin tile]
  (local collidable (. mapin.tile-set tile.type :collidable))
  (local [x y] [(* 32 tile.x) (* 32 tile.y)])
  (when (= tile.type :ground) (world:add tile x y 32 32)))

;; create map
(fn col.new [mapin player objects]
  (local world (bump.newWorld 32))
  (local tiles mapin.data.ground)
  (each [id tile (pairs tiles)]
    (col.add-tile world mapin tile))
  (world:add player
             (+ player.col.x player.pos.x)
             (+ player.col.y player.pos.y)
             player.col.w
             player.col.h player.collision-type)
  (each [id object (pairs objects)]
    (world:add object
               (+ object.col.x object.pos.x)
               (+ object.col.y object.pos.y)
               object.col.w
               object.col.h
               object.collision-type))

  world)

col
