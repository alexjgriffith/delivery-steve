(local levels {})

(tset levels :list
      {:opening {:name "opening"
                     :loading-screen "It Begins Again"
                     :display "The Beginning"
                    :description ""
                    :score 0
                     :next-level "tutorial-1"
                    }
       :tutorial-1 {:name "tutorial-1"
                     :loading-screen "The First Day Dawns"
                     :display "Day 1"
                    :description ""
                    :score 0
                     :next-level "tutorial-2"
                    }
       :tutorial-2 {:name "tutorial-2"
                     :loading-screen "The Second Day Dawns"
                     :display "Day 2"
                    :description ""
                    :score 0
                     :next-level "tutorial-3"
                    }
       :tutorial-3 {:name "tutorial-3"
                     :loading-screen "The Third Day Dawns"
                     :display "Day 3"
                    :description ""
                    :score 0
                     :next-level "level-1"
                    }
       :level-1 {:name "level-1"
                    :loading-screen "The Fourth Day Dawns\nThree Remain"
                 :display "Day 4"
                 :description ""
                 :score 0
                 :next-level "level-2"
                 }
       :level-2 {:name "level-2"
                 :loading-screen "The Fifth Day Dawns"
                 :display "Day 5"
                 :description ""
                 :score 0
                 :next-level "level-3"
                 }
       :level-3 {:name "level-3"
                 :loading-screen "The Penultimate Morning Comes"
                 :display "Day 6"
                 :description ""
                 :score 0
                 :next-level "level-4"
                 }
       :level-4 {:name "level-4"
                 :loading-screen "The Penultimate Morning Comes"
                 :display "Day 7"
                 :description ""
                 :score 0
                 :next-level "level-5"
                 }
       :level-5 {:name "level-5"
                 :loading-screen "The Penultimate Morning Comes"
                 :display "Day 8"
                 :description ""
                 :score 0
                 :next-level "level-6"
                 }
       :level-6 {:name "level-6"
                 :loading-screen "The Penultimate Morning Comes"
                 :display "Day 9"
                 :description ""
                 :score 0
                 :next-level "final-level"
                 }
       :final-level {:name "final-level"
                     :loading-screen "The Final Sun Rises"
                     :display "You Did It!"
                     :description ""
                     :score 0
                     :next-level "post-game"
                     }
       :post-game {:name "post-game"
                   :loading-screen "I Think You've Done It"
                   :display "Credits"
                   :description ""
                   :score 0
                   :next-level "opening"
                   }
       })

(fn levels.display [level]
  (. levels :list level :display))

(fn levels.loading-screen [level]
  (if level
      (do (local next-level (. levels :list level :next-level))
          (. levels :list next-level :loading-screen))
      (. levels :list :data-tutorial-1 :loading-screen))) ;; update on rename

;; (fn _G.newmap [name]
;;   (local map (require :lib.map))
;;   (local level ((require :level) level _G.assets.sprites.delivery-steve-tiles))
;;   (level:save (map.new) (.. name ".fnl")))

(fn levels.load [state level-name from-clipboard revert]
  (when (. levels.list level-name)
    (tset (. levels.list level-name) :score 0))
  (local objects (require :objects))
  (local player (require :prefab-player))
  (local camera (require :prefab-camera))
  (local col (require :prefab-col))
  (local ui (require :prefab-ui))
  (local particles (require :particles))
  (levels.unload state)
  (tset state :pointer {:i 0 :j 0})
  (tset state :level ((require :level) level-name ;;(. levels.list level-name :name)
                      _G.assets.sprites.delivery-steve-tiles from-clipboard revert))
  (local steve (. state.level.map.data.players "steve"))
  (tset state :steve (player.new steve.x steve.y 32 64 steve.flipped)) ;;; todo
  (state.steve:lock-movement)
  (tset state :objects (objects.gen-objects state.level.map.data.objects))
  (objects.pickup-packs state.objects)
  (tset state :icons (objects.gen-icons state.objects))
  (tset state :world (col.new state.level.map state.steve state.objects))
  (tset state :particles particles)
  (tset state :resources {})
  (each [_ obj (pairs state.objects)]
    (when (and obj.type (= obj.type :text))
      (obj:init)
      )
    )
  (tset state :camera (camera.new state.steve.pos))
  (state.camera:update 0)
  (tset state :ui (ui.new))
  )

(fn levels.revert [state]
  (local next-level state.current-level)
  (levels.unload state)
  (levels.load state next-level nil :revert)
  (tset state :current-level next-level))

(fn levels.load-from-clipboard [state]
  (local next-level state.current-level)
  (levels.unload state)
  (levels.load state next-level :load-from-clipboard)
  (tset state :current-level next-level))

(fn levels.reload [state]
  (local next-level state.current-level)
  (levels.unload state)
  (levels.load state next-level)
  (tset state :current-level next-level))

(fn levels.next [state]
  (local next-level (or (. levels :list state.current-level :next-level)
                        :opening))
  (levels.unload state)
  (tset state :current-level next-level)
  (levels.load state next-level))

(fn levels.previous [state]
  (var next-level :turorial-1)
  (each [key value (pairs levels.list)]
    (when (= value.next-level state.current-level)
      (set next-level key)))
  (levels.unload state)
  (levels.load state next-level)
  (tset state :current-level next-level))

(fn levels.unload [state]
  (local ret {:steve (when state.steve (lume.clone state.steve))
              :objects (when state.objects (lume.clone state.objects))
              :icons (when state.icons (lume.clone state.icons))
              :level (when state.level (lume.clone state.level))})
  (tset state :steve nil)
  (tset state :objects nil)
  (tset state :icons nil)
  (tset state :level nil)
  (tset state :pointer nil)
  (tset state :world nil)
  (tset state :ui nil)
  (tset state :resources {})
  (tset state :particles nil)
  ;; (when (not false) (collectgarbage))
  ;;ret
  nil
  )

levels
