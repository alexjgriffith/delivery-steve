(local bump (require :lib.bump))

(local params (require :params))

(local timer (require :lib.timer))

(fn check-key-down [key]
  (local state (require :state))
  (if (-> state.keys
          (. key)
          (lume.map love.keyboard.isDown)
          (lume.reduce (fn [a b] (or a b))))
      1
      0))

(fn is-player-on-ground [player]
  (local world (. (require :state) :world))
  (when (world:hasItem player)
    (local (actual-x actual-y cols len)
         (world:check player (+ player.pos.x player.col.x ) (+ player.pos.y player.col.y 1)
                                      (fn [item other]
                                         (match other.type
                                           :ground (do  :slide)
                                           _ nil))))
  (set player.pos.x (- actual-x player.col.x))
  (set player.pos.y (- actual-y player.col.y))
  (> len 0))
  )

(fn is-player-on-roof [player]
  (local world (. (require :state) :world))
  (when (world:hasItem player)
  (local (actual-x actual-y cols len)
         (world:check player
                      (+ player.pos.x player.col.x)
                      (+ player.pos.y 20)
                                      (fn [item other]
                                         (match other.type
                                           :ground (do  :slide)
                                           _ nil))))
  (when (> len 0)
    (set player.speed.y 0)
    ;; (set player.pos.y actual-y)
    )
  ;; (set player.pos.x actual-x)

  (> len 0)))

(fn number-of-packages [player]
  (# player.packages))

(fn set-run-speed [player]
  (local params (require :params))
  (let [pack-count (number-of-packages player)
        max-speed
        (match pack-count
          0 30
          1 25
          2 20
          3 20
          4 15
          )
        rate
        (if (> (number-of-packages player) 0) 30 50)
        decay
        (match pack-count
          0 240
          1 240
          2 200
          3 120
          4 70)]
    (tset params.player-speed.walk :rate rate)
    (tset params.player-speed.walk :decay decay)
    (tset params.player-speed.walk :max max-speed)
    (tset params.player-speed.walk :max-s2 (/ max-speed (math.sqrt 2)))))


(fn set-jump-counter [player]
  (local params (require :params))
  (if (> (number-of-packages player) 1)
      (tset params.player-speed.jump :count 1)
      (tset params.player-speed.jump :count 2)
      ))

(var previous-packs 0)
(fn set-jump-timer [player]
  (local params (require :params))
  (let [pack-count (number-of-packages player)]
    (if (> pack-count 2)
      (tset params.player-speed.jump :timer 0.25)
      (tset params.player-speed.jump :timer 0.5)
      )
  (when (~= previous-packs pack-count)
    (set player.controler.jump.timer (timer params.player-speed.jump.timer)))
  (set previous-packs pack-count))
  )

(fn set-jump-block [player]
(local params (require :params))
  (if (> (number-of-packages player) 3)
      (tset params.player-speed.jump :power -20)
      (tset params.player-speed.jump :power -71)))

(fn limit-movement [player]
  (set-run-speed player)
  (set-jump-counter player)
  (set-jump-timer player)
  (set-jump-block player))

(fn is-jump-key-down [player]
  (= (check-key-down :jump) 1))

(fn is-jump-over [player]
  (player.controler.jump.timer:over))

(fn is-jump-released [player]
  player.controler.jump.released)

(fn is-jump-count<2 [player]
  (< player.controler.jump.count params.player-speed.jump.count))

(fn is-jump-current-state [player]
  (local current-state player.state-machine.current-state)
  (= current-state :jump))

(fn jump-update [player dt]
  (player.controler.jump.timer:update dt))

(fn jump-reset [player dt]
  ;; do it like this so when params is updated so is the timer
  (set player.controler.jump.timer (timer params.player-speed.jump.timer))
  (tset player.controler.jump :count 0))

(fn jump-reset-timer [player dt]
  ;; do it like this so when params is updated so is the timer
  (player.controler.jump.timer:reset))

(fn jump-check-release [player]
  (when (not (is-jump-key-down))
    (set player.controler.jump.released true)))

(fn [player state dt]
  (local pos player.pos)
  (local current-state player.state-machine.current-state)

  ;; Update State
  (var next-state :fall)
  (let [on-ground (is-player-on-ground player)
        on-roof (is-player-on-roof player)
        jump-key-down (is-jump-key-down player)
        jump-over (is-jump-over player)
        jump-released (is-jump-released player)
        jump-count<2 (is-jump-count<2 player)
        jump-current-state (is-jump-current-state player)]
    (when on-ground
      (set next-state :walk))
    (when (and (not on-ground) (or jump-over (not jump-key-down)))
      (set next-state :fall))
    (when (and (and jump-key-down (not jump-over))
               (or jump-current-state jump-released)
               (or jump-current-state jump-count<2)
               (not on-roof))
      (set next-state :jump))

    )

  ;;
  (limit-movement player)

  ;; Triggers when state changes
  (when (~= next-state player.state-machine.current-state)
    (match current-state
      :jump (jump-reset-timer player dt)
      :fall :nothing
      :walk :nothing)
    (match next-state
      :jump (do
                ;; reset timers
                ;; increment jump.count
                (set player.controler.jump.count (+ player.controler.jump.count 1))
                (set player.controler.jump.released false)
                (set player.speed.y params.player-speed.jump.power)
                (player.controler.jump.timer:reset)
              )
      :fall (do :nothing)
      :walk (do (jump-reset player dt))
      ))

  ;; Global Updates
  (jump-check-release player)
  ;; State Specific Updates
  (match next-state
      :jump (jump-update player dt)
      :fall (do :nothing)
      :walk (do :nothing)
      )
  (love.event.push :state-change current-state next-state)
  (set player.state-machine.current-state next-state)
  ;; return previous state
  current-state)
