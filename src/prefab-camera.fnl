(local camera {})

(fn simple [camera]
  (local state (require :state))
  (local steve (. state :steve))
  (tset camera :x (- (- steve.pos.x) 16 (/ camera.width -2 camera.scale)))
  (tset camera :y (- (- steve.pos.y) -20 16 (/ camera.height -2 camera.scale)))
  )

(fn lag [camera dt]
  (local state (require :state))
  (local steve (. state :steve))
  (local dx (- steve.pos.x camera.target.x))
  (local dy (- steve.pos.y camera.target.y))
  (local k (* 60 0.1))
  (set camera.target.x (+ camera.target.x (* dx k dt)))
  (set camera.target.y (+ camera.target.y (* dy k dt)))
  (tset camera :x (- (- camera.target.x) 16 (/ camera.width -2 camera.scale)))
  (tset camera :y (- (- camera.target.y) -20 16 (/ camera.height -2 camera.scale)))
  )

(fn lag-lead [camera dt]
  (local state (require :state))
  (local steve (. state :steve))
  (local oy (match steve.state-machine.current-state
              :fall 1
              :jump 1
              :walk 0
              ))
  (local dx (- (+ steve.pos.x (* -20 steve.flipped)) camera.target.x))
  (local dy (- (+ steve.pos.y 0) camera.target.y))
  (local kx (* 60 0.1))
  (local ky (* 60 0.5))
  (set camera.target.x (+ camera.target.x (lume.clamp (* dx kx dt) (- (math.abs dx))
                                                      (math.abs dx)
                                                      )))

  (set camera.target.y (+ camera.target.y (lume.clamp (* dy ky dt) (- (math.abs dy))
                                                      (math.abs dy)
                                                      )))

  (tset camera :x (- (- camera.target.x) 16 (/ camera.width -2 camera.scale)))
  (tset camera :y (- (- camera.target.y) -20 16 (/ camera.height -2 camera.scale)))
  )

(fn sonic [camera]
    (local state (require :state))
  (local steve (. state :steve))
  ;; (tset camera.target :x (- (- state.steve.pos.x) 16 (/ camera.width -2 camera.scale)))
  ;; (tset camera.target :y (- (- state.steve.pos.y) -20 16 (/ camera.height -2 camera.scale)))
  (when (< steve.pos.x (- camera.box.x camera.box.w))
    (tset camera.box :x  (+ state.steve.pos.x camera.box.w)))
  (when (> steve.pos.x (+ camera.box.x camera.box.w))
    (tset camera.box :x  (- state.steve.pos.x camera.box.w)))
  ;;(when (or (< steve.pos.y camera.box.y) (> steve.pos.y (+ camera.box.y camera.box.h)))
  (when (< steve.pos.y (- camera.box.y camera.box.h))
    (tset camera.box :y  (+ state.steve.pos.y camera.box.h)))
  (when (> steve.pos.y (+ camera.box.y camera.box.h))
    (tset camera.box :y  (- state.steve.pos.y camera.box.h)))


  (tset camera :x (- (- camera.box.x) 16 (/ camera.width -2 camera.scale)))
  (tset camera :y (- (- camera.box.y) -20 16 (/ camera.height -2 camera.scale))))

(local state (require :state))
(fn camera.update [camera dt]
  ((. {:lag-lead lag-lead
       :sonic sonic} state.camera-type) camera dt))

(local camera-mt {:__index camera})

(fn camera.new [pos]
  (local new (setmetatable {:x pos.x :y pos.y
                            :box {:x pos.x :y pos.y :w 12 :h 6}
                            :target {:x pos.x :y pos.y}
                            :step 0.3 :first false :scale 3 :width 1280 :height 720} camera-mt))
  (new:update 100)
  new
  )

camera
