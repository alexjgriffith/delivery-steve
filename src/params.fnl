(local colour (require :lib.colour))

{:colours {:sky (colour.hex-to-rgba "#c5ccb8")
           :ui-back (colour.hex-to-rgba "#6f6776")
           :ui-back-2 (colour.hex-to-rgba "#919197")
           :ui-text (colour.hex-to-rgba "#433455")
           :sky-text (colour.hex-to-rgba "#666092")
           :blue   (colour.hex-to-rgba "#5fcde4")
           :black  (colour.hex-to-rgba "#222034")
           :red    (colour.hex-to-rgba "#ac3232")
           :grey   (colour.hex-to-rgba "#595652")
           :blue-grey   (colour.hex-to-rgba "#9babb7")
           :white  (colour.hex-to-rgba "#ffffff")
           :pink  (colour.hex-to-rgba "#d77bba")
           :background  (colour.hex-to-rgba "#222034")
           :light-yellow  (colour.hex-to-rgba "#cbdbfc")
           :text  (colour.hex-to-rgba "#222034")
           }
 :screen-width 1280
 :screen-height 720
 :w2 640
 :h2 360
 :keys {:left ["left" "a"]
        :right ["right" "d"]
        :up ["up" "w" "space"]
        :down ["down" "s"]}
 :player-speed {
                :fall {:rate 30
                       :decay 120
                       :gravity  500
                       :fall-max 100
                       :max 25
                       :max-s2 (/ 150 (math.sqrt 2))
                       :floor 4}
                :walk {:rate 30
                       :decay 20
                       :max 3.1
                       :max-s2 (/ 2 (math.sqrt 2))
                       :floor 4}
                :jump {:rate 30
                       :decay 120
                       :timer 0.5
                       :count 2
                       :power -71
                       :gravity 60
                       :jump-max 100
                       :max 25
                       :max-s2 (/ 150 (math.sqrt 2))
                       :floor 4}
                }
 }
