{:tutorial-1 [{:text "Use A and D to Move"}
              {:text "Use Space to Pickup"}
              {:text "Use W to Jump"}
              {:text "Press W Again"}
              {:text "More Packages = Fewer Abilities"}
              {:text "Run then Jump"}
              {:text "Drop Off With Space"}
              {:text "Fewer Abilities = More Points"}
              {:text "Fewer Abilities = More Points"}
              {:text "Hold W for a"}
              {:text "Carrying More Weighs"}
              {:text "You Down"}
              {:text "Super Jump"}
              {:text "for a Double Jump"}
              ]
 :tutorial-2 [{:text "! Means They Have a Package"}
              {:text ":( Means They Want a Package"}
              {:text "Take With Space"}
              {:text "Drop Off With Space"}
              {:text "Respawn at Recent House"}
              {:text "Respawn at Recent House"}
              ]

 :tutorial-3 [{:text "Packages for Jill and Hank"}
              {:text "Fewer Abilities = Higher Rewards"}
              {:text "Hold W for a Long Jump"}
              {:text "Run then Jump"}
              {:text "People Pay More For Delivery"}
              {:text "Dropping Packs Drops Your Score"}
              {:text "Respawn at Recent House"}
              {:text "Respawn at Recent House"}
              ]

 :level-1 [{:text "Leap of Faith"}
           {:text "Run then Jump"}
           {:text "Two Ws Two Jumps"}
           {:text "The World Has Been Torn Apart"}
           {:text "Each Day You Wake"}
           {:text "to Find Your Town"}
           {:text "Jumbled"}
           {:text "Jumbled"}
           {:text "This has Complicated Your Job"}
           {:text "as a Delivery Man a Scootch"}
           {:text "But, You'll Manage"}
           {:text "You are Delivery Steve!"}
           ]
  :level-2 [{:text "There is a Legend"}
            {:text "of a Saviour"}
            {:text "A Man Who Would Deliver"}
            {:text "SO Many Packages that"}
            {:text "The World Would Be"}
            {:text "Set Right"}
            {:text "Could You be That"}
            {:text "Saviour Steve?"}
            {:text "Saviour, Steve?"}
            ]
 :level-3 [{:text "Today is K's Birthday"}
           {:text "Where is her Present STEVE!?"}
           {:text "Birthday Present"}
           {:text "Delivery. Classic"}
           {:text "Saviour Material"}
           ]
  :level-4 [{:text "Unfortunately Carl has"}
            {:text "Misplaced His House"}
            {:text "WOW, you found the"}
            {:text "House, Steve."}
            {:text "Maybe this Saviour"}
            {:text "Thing is Real?"}
            {:text "Now Back to Work!"}
            {:text "Now Back to Work!"}
            {:text "Sounds Like a Job for"}
            {:text "Delivery Steve!"}
            ]
 :level-5 [{:text "The Whole Town's Together"}
           {:text "The Deliveries are working!"}
           {:text "Keep Up"}
           {:text "the Good Work,"}
           {:text "Steve!"}
           ]
 :level-6 [{:text "Has Anyone Seen Mike?"}
           {:text "Hiding in a Cave!"}
           {:text "Classic Mike"}
           {:text "I Mean Really"}
           {:text "Where is Mike?"}
           {:text "Hope Mike's OK!"}
           {:text "Hello? Mike?"}
           {:text "Hello? Mike?"}
           {:text "Better Find Him Steve!"}
           {:text "A Package for Mike?"}
           {:text "Now You Really Have"}
           {:text "to Find Him!"}
           ]
 :final-level [{:text "After 9 Days of Terror"}
               {:text "Your Little Town Looks"}
               {:text "Like its Back to Normal"}
               {:text "Were You the Saviour Steve?"}
               {:text "Probably Not, But its Nice to Dream"}
               ]
 :post-game [{:text "Game Music and Art by:"}
             {:text "Alexander Griffith"}
             {:text "Language (Fennel)"}
             {:text "Calvin Rose (MIT/X11)"}
             {:text "Engine (LÖVE)"}
             {:text "LÖVE Dev Team (Zlib)"}
             {:text "Font (FFFORWA)"}
             {:text "Fonts For Flash"}
             {:text "Library (Lume)"}
             {:text "Libraries (Lume, Bump, ANIM8)"}
             {:text "Enrique Cota, RXI (MIT)"}
             {:text "Sound Effects (CCBY)"}
             {:text "NeadSimic, Nicole Marie, Jalastram"}
             {:text "Blender Foundation, MentalSanityOff"}
             {:text "Thanks for Playing!"}
             {:text "Delivery Steve"}
             ]}
