(local post-office {})

(fn post-office.collide [post-office])

(fn post-office.update [self dt]
    (tset self.animation :timer (+ dt self.animation.timer))
  (when (> self.animation.timer self.animation.period)
    (tset self.animation :timer 0)
    (tset self.animation :index (+ self.animation.index 1)))
  (when (> self.animation.index (# self.animation.frames))
    (tset self.animation :index 1)))

(fn post-office.draw [self _ editor?]
  (local current-level (. (require :state) :current-level))
  (love.graphics.setColor 1 1 1 1)
  (local quad self.post-office-quad)
  (local name-quad self.name-quad)
  (local {: index : frames} self.animation)
  (local oy (. frames index))
  (if editor?
      (love.graphics.draw self.image quad -15 2 0 0.5)
      (do
        (when  (and (~= :final-level current-level)
                    (~= :post-game current-level))
            (love.graphics.draw self.image name-quad self.pos.x (- self.pos.y 30 (- oy))))
        (love.graphics.draw self.image quad (+ self.root.x self.pos.x)
                        (+ self.root.y self.pos.y)))))

(local post-office-mt {:__index post-office
                :update post-office.update
                :collide post-office.collide
                       :draw post-office.draw})

(fn give-package [self]
  (fn [player]
    (local packages player.packages)
    (db ["trying to drop off package" player.packages self.short-name])
    (local {: short-name : char-names : name-map} self)
    (var success false)
    (each [key pack (pairs packages)]
      (let [pack-name (. char-names (. name-map pack))]
        (db pack-name)
        (when (and (not success)
                   (= pack-name short-name))
          (lume.remove  packages pack)
          (table.insert self.packages pack)
          (local pack-module (require :prefab-pack))
          (pack-module.set-delivered pack)
          (set success :post-office-pack))))
    (if success
        (db "success")
        (db "failure")
        )
    success))


(fn post-office.new [atlas x y]
  (fn new-quad [member]
    (let [{: x : y : w : h} (. atlas.param.frames member :frame)]
      (love.graphics.newQuad  x y w h 512 512)))
  (local post-office-quad (new-quad :post-office ))
  (local name-quad (new-quad :text-post-office))
  (local name-map {:k 1 :jill 2 :aly 3 :sara 4
                   :tom 5 :mike 6 :carl 7 :hank 8 :p1 9
                   :p2 9 :p3 9 :p4 9 :p5 9 :p6 9 :p7 9 :p8 9})
  (local char-names [:k :jill :aly :sara
                     :tom :mike :carl :hank :post-office])
  (local ret
         (setmetatable {:pos {: x : y}
                        :image atlas.image
                        : post-office-quad
                        : name-quad
                        : name-map
                        : char-names
                        :root {:x 8 :y 32}
                        :x x :y y ;; origional position
                        :w 160 :h 80
                        :collision-type :cross
                        :col {:x 0 :y -30 :w 160 :h 130}
                        :type :post-office
                        :name :post-office
                        :short-name :post-office
                        :root {:x 0 :y +3}
                        :packages []
                        :animation {:timer (lume.random 0.1 0.29) :index (math.floor (lume.random 2 7)) :frames [2 2 2 2 2 2 2 2 1 1 1] :period 0.3}
                        :active true} post-office-mt))
  (tset ret :action (give-package ret))
  ret
  )

post-office
