(local game {})

(local gamestate (require :lib.gamestate))
(local state (require :state))
(local levels (require :levels))
(local col (require :prefab-col))
(local editor (require :editor))
(local gif ((require :lib.save-gifs) "example"))
(local params (require :params))
(local floaty (require :floaty))

(fn update-pointer []
  (local camera state.camera)
  (local pointer state.pointer)
  (local (ix iy) (love.mouse.getPosition))
  (local [x y] [ (/ (- ix offsets.gx) offsets.s) (/ (- iy offsets.gy) offsets.s)])
  (local tile-size 16)
  (tset pointer :i  (math.floor (/ (- (/ x camera.scale) camera.x) tile-size)))
  (tset pointer :j  (math.floor (/ (- (/ y camera.scale) camera.y) tile-size)))
  )

(local canvas (love.graphics.newCanvas 1280 720))
(canvas:setFilter :nearest :nearest)
(var objs nil)
(var len nil)
(fn game.draw [self]
  (local {: level  : camera : world  : steve} state)
  (love.graphics.push "all")
  (when (not (or state.web (not state.fullscreen)))
    (love.graphics.setCanvas canvas))
  (love.graphics.clear)
  (love.graphics.setColor params.colours.sky)
  (love.graphics.rectangle "fill" 0 0 1280 720)
  (love.graphics.setColor 1 1 1 1)
  (love.graphics.scale camera.scale)
  (love.graphics.translate camera.x camera.y)
  (love.graphics.draw level.tileset-batch)

  (if state.fast-render
    (do
    (for [i 1 len]
      (let [object (. objs i)]
      (when (or (~= object.type :characters) (~= object.type :pack))
        (object:draw))))
    (for [i 1 len]
      (let [object (. objs i)]
        (when  (= object.type :characters)
          (object:draw))))
    (steve:draw)
    (for [i 1 len]
      (let [object (. objs i)]
        (when  (= object.type :pack)
          (object:draw)))))

  (do
    (local objs  state.objects)
    (each [_ object (pairs objs)]
      (when (or (~= object.type :characters) (~= object.type :pack)) (object:draw)))
    (each [_ object (pairs objs)]
      (when (= object.type :characters) (object:draw)))
    (love.graphics.setColor 1 1 1 1)
    (steve:draw)
  (each [_ object (pairs objs)]
    (when (= object.type :pack) (object:draw)))))


  (when state.colliders (col.draw state.world))
  (state.particles:draw)
  (love.graphics.pop)
  (when (not (or state.web (not state.fullscreen)))
    (love.graphics.setCanvas canvas))
  (when (not state.editor)
    (local overlay (require :overlay))
    (overlay:draw)
    (state.ui:draw))
  (when (or state.fps state.dev) (love.graphics.printf (love.timer.getFPS ) 1240 700 40 :right))
  (update-pointer)
  (when state.editor (editor.draw state))
  (when (not (or state.web (not state.fullscreen)))
    (love.graphics.setCanvas)
    (love.graphics.draw canvas offsets.gx offsets.gy 0 offsets.s))
  (floaty.draw)
  ;; (love.graphics.printf (pp (require :bit)) 10 10 1260)
  )

(local update-resources (require :resources))

(fn game.update [self dt]
  (local {: world  : objects : steve : camera} state)
  (update-resources)
  (if state.fast-render
    (do (set (objs len) (world:queryRect (- steve.pos.x 250) (- steve.pos.y 130)
                                         500 260
                                         #(and (~= $1.type :player) (~= $1.type :ground))))
        (for [i 1 len] (: (. objs i) :update dt)))
    (each [_ object (pairs state.objects)] (when object (object:update dt))))
  (state.particles:update dt)
  (steve:update dt)
  (camera:update dt)
  (when state.editor (editor.update dt))
  (state.ui:update dt)
  (local overlay (require :overlay))
  (overlay:update dt)
  (when state.dev (gif:update dt))
  (floaty.update dt)
  (when (not false) (collectgarbage))
  )

(fn game.leave [self ...]
  ;; (love.mouse.setVisible true)
  )

(fn game.enter [_self _from reset?]
  (local steve (. (require :state) :steve))
  (match reset?
    :resume :good-to-go
    _ (levels.load state state.current-level)))

(fn game.mousereleased [self ix iy button]
  (db "mouse-released")
  (local [x y] [ (/ (- ix offsets.gx) offsets.s)   (/ iy (- offsets.s offsets.gy))])
  (local overlay (require :overlay))
  (if (not state.editor)
      (overlay:mousereleased x y button)
      (editor.mousereleased self x y button state)))

(fn game.mousepressed [self ix iy button]
  (db "mouse-pressed")
  (local [x y] [ (/ (- ix offsets.gx) offsets.s)   (/ iy (- offsets.s offsets.gy))])
  (local overlay (require :overlay))
  (if (not state.editor) (overlay:mousepressed x y button)
      (editor.mousepressed self x y button state)))


(fn toggle-vsync [value]
  (local (w h f) (love.window.getMode) )
  (tset f :vsync value)
  (love.window.setMode w h f ))

(fn game.keypressed [self key code]
  (local {: world} state)
  (match key
    :space (state.steve:action)
    :escape (let [overlay (require :overlay)] (overlay.escape))
    :return (let [overlay (require :overlay)] (overlay.enter))
    :f10 (toggle-fullscreen)
    :q   (when (not state.web) (screenshot))
    :1   (if state.fps (tset state :fps false) (tset state :fps true))
    :2   (if state.fast-render (tset state :fast-render false) (tset state :fast-render true))
    :3   (if state.dev (tset state :dev false) (tset state :dev true))
    :4 (when (not state.web) (toggle-vsync true))
    :5 (when (not state.web) (toggle-vsync false))
    :6 (if (not (= state.camera-type :lag-lead))
           (set state.camera-type :lag-lead)
           (set state.camera-type :sonic))
    :tab (when (and state.dev (not state.web))
           (if state.editor (do
                              (tset (. (require :state) :camera) :scale 3)
                              (set state.editor false))
               (do
                 (tset (. (require :state) :camera) :scale 1)
                 (set state.editor true))))
    :. (when (and state.dev (not state.web))
         (if state.colliders (set state.colliders false) (set state.colliders true)))
    :0 (when state.dev (let [overlay (require :overlay)] (pp "load final level") (overlay.load-level :final-level)))
    :- (when state.dev (let [overlay (require :overlay)] (pp "load post-game") (overlay.load-level :post-game)))
    :p (when (and state.dev (not state.web))
         (assets.sounds.click:play)
         (state.level:save state.level.map state.current-level))
    :l  (when (and state.dev (not state.web))
          (assets.sounds.click:play)
          (do (db "reset-begin")
                (levels.load  state state.current-level nil true)
                (state.steve:unlock-movement)
                (db "reset-complete")))
    :c   (when (and state.dev (not state.web))
           (do (assets.sounds.bounce:play) (when state.dev (gif:start))))
    :v   (when (and state.dev (not state.web))
           (do (assets.sounds.bounce:play) (when state.dev (gif:stop))))
    _ (when (and state.dev (not state.web))
        (editor.keypressed self key code state))
    ))

game
