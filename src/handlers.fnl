(local state (require :state))

(local gamestate (require :lib.gamestate))

(local col (require :prefab-col))
(local objects (require :objects))

(local particles (require :particles))

(fn love.handlers.state-change [previous-state current-state]
  (local state (require :state))
  (local steve (. state :steve))
  (match [previous-state current-state]
    [:fall :walk] (when assets ;; land
                    (assets.sounds.land:setPitch 1)
                    (assets.sounds.land:setVolume 0.1)
                    (assets.sounds.land:play)
                    (particles.new :land (+ steve.pos.x 16) (+ steve.pos.y 32))
                    )
    )
  )


(fn love.handlers.action [action value]
  ;; :put-down-pack
  ;; :pick-up-pack
  ;; :take-pack
  ;; :give-pack
  ;; :post-office-pack
  (local steve (. state :steve))
  (local floaty (require :floaty))
  (floaty.new value)
  (match action
    :put-down-pack (when assets
                    (assets.sounds.bounce:setPitch 1)
                    (assets.sounds.bounce:setVolume 0.3)
                    (assets.sounds.bounce:play)
                    ;; (particles.new :land (+ steve.pos.x 16) (+ steve.pos.y 32))
                    )
    :pickup-pack (when assets
                    (assets.sounds.bounce:setPitch 1)
                    (assets.sounds.bounce:setVolume 0.3)
                    (assets.sounds.bounce:play))
    :take-pack (when assets
                 (assets.sounds.bounce:setPitch 1)
                 (assets.sounds.bounce:setVolume 0.3)
                 (assets.sounds.bounce:play))

    :give-pack (when assets
                 (assets.sounds.cash:setPitch 1)
                 (assets.sounds.cash:setVolume 0.1)
                 (assets.sounds.cash:play))

    :post-office-pack (when assets
                 (assets.sounds.cash:setPitch 1)
                 (assets.sounds.cash:setVolume 0.1)
                 (assets.sounds.cash:play))
    )
  )

(fn love.handlers.transition [from]
  (when state.level.over
    (tset state.level :transitioning true)
    (assets.sounds.page:play)
    ;; (tset state :ui nil)
    (gamestate.switch (require :mode-transition))))


(fn love.handlers.edit-map [instruction tile tile-size ?previous]
  (local world state.world)
  (when world
    (match instruction
      :add-tile (col.add-tile world state.level.map tile)
      :replace-tile (do
                      (local (items len) (world:getItems))
                      (each [_ item (pairs items)]
                        (when (and (= item.x tile.x) (= item.y tile.y))
                          (world:remove item)))
                      (col.add-tile world state.level.map tile))
      :remove-tile (world:remove tile))))

(fn love.handlers.edit-obj [instruction obj]
  ;; should just call a system! ces.systems.map.edit
  (local world state.world)
  ;; (local level state.entity.level)
  (when (and world state.objects obj)
    (match instruction
      :add-object (do
                    (each [key value (pairs (objects.gen-objects [obj]))]
                      (let [object (. state.objects key)]
                        (when (and object (world:hasItem object)) (world:remove object))
                        (tset state.objects key nil)
                        (tset state.icons key nil))
                      (tset state.objects key value)
                      (world:add value (+ value.pos.x value.col.x) (+ value.pos.y value.col.y) value.col.w value.col.h))
                    (local all-icons (objects.gen-icons state.objects))
                    (each [key icon (pairs state.icons)]
                      (tset (. all-icons key) :active icon.active))
                    (tset state :icons all-icons))
      :remove-object (each [_ object (pairs state.objects)]
                     (when (and (= object.x obj.x) (= object.y obj.y))
                       (when (and object (world:hasItem object)) (world:remove object))
                       (tset state.objects object.name nil)
                       (tset state.icons object.name nil))
                     ))))
