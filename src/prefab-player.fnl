(local player {})

(local params (require :params))
(local timer (require :lib.timer))

(local animations {:idle {:frames [1 2] :rate 900 :type :repeat}
                   :talk {:frames [2 3] :rate 300 :type :repeat}
                   :walk {:frames [4 5 6 7 8 9] :rate 100 :type :repeat}
                   :run {:frames [10 11 12 13] :rate 200 :type :repeat}
                   :jump {:frames [14] :rate 300 :type :hold}
                   :fall-up {:frames [15] :rate 100 :type :hold}
                   :fall-mid {:frames [16] :rate 100 :type :hold}
                   :fall-down {:frames [17] :rate 100 :type :hold}
                   :land {:frames [18 19] :rate 100 :type :single}
                   :jump-forward {:frames [20] :rate 100 :type :hold}
                   :fall-forward {:frames [21] :rate 100 :type :hold}})

(fn check-key-down [key]
  (local state (require :state))
  (if (-> state.keys
          (. key)
          (lume.map love.keyboard.isDown)
          (lume.reduce (fn [a b] (or a b))))
      1
      0))

(fn animation-state-mapping [player state previous-state]
  (match [state previous-state player.speed.y player.speed.x player.animation.animation-state]
    [:walk :fall _ _ ] :land
    [:walk _ _ _ :land] :land
    ([:jump _ _ x] ? (~= 0 x)) :jump-forward
    [:jump _ _ _] :jump
    ([:walk _ _ x] ? (or (< x -20) (> x 20))) :run
    ([:walk _ _ x] ? (~= 0 x)) :walk
    [:walk _ _ _] :idle
    ([:fall _ y _] ? (< y -1))  :fall-up
    ([:fall _ y _] ? (> y 1)) :fall-down
    [:fall _ _ _] :fall-mid
    ))

(fn direction [player state]
  (match [state  player.speed.x]
    ([_ x] ? (and (> x -0.5) (< x 0.5)))  player.flipped
    [:jump  _] player.flipped
    ([:walk  x] ? (< x -0.4 )) 1
    ([:walk  x] ? (> x 0.4 )) -1
    [:walk _]  player.flipped
    ([:fall x] ? (= 0 x)) player.flipped
    ([:fall  x] ? (> 0 x)) 1
    ([:fall  x] ? (< 0 x)) -1
    ))

(local object-actions {:characters true
                       :pack true
                       :post-office true})

(local action-orders {:characters 4 :packs 3 :post-office 2 :houses 1})

(fn action-sort [a b]
    (let [va (or (. action-orders a.type) 0)
          vb (or (. action-orders b.type) 0)]
      (< va vb)))

(fn put-down-package [player]
  (var ret false)
  (when
      (and (> (# player.packages) 0)
           (= :walk player.state-machine.current-state))
    (let [name (. player.packages (# player.packages))
          state (require :state)]
      (each [_ object (pairs state.objects)]
        (when (and (= object.type :pack)
                   (= object.number name))
          (do (object:place player.pos.x player.pos.y)
              (table.remove player.packages (# player.packages))
              (set ret :put-down-pack))))
      ))
  ret)

(fn at-house? [player]
  (local state (require :state))
    (let [{: pos : col} player
        x (+ pos.x col.x)
        y (+ pos.y col.y)
        w col.w
        h col.h]
      (local (items len) (state.world:queryRect
                          x y w h
                          #(and (. $1 :type)
                                (or (= (. $1 :type) :houses) (= (. $1 :type) :post-office))
                                )))
      (when (and (> len 0) (= :walk player.state-machine.current-state))
        (tset player.reset.pos :x player.pos.x)
        (tset player.reset.pos :y player.pos.y)
        ))
  )

(fn player.action [player]
  (local state (require :state))
  (let [{: pos : col} player
        x (+ pos.x col.x)
        y (+ pos.y col.y)
        w col.w
        h col.h]
    (local (items len) (state.world:queryRect
                        (+ (* player.flipped -8) x) y w h
                        #(and (. $1 :type)
                              (. $1 :action)
                              (. object-actions (. $1 :type)))))
    (local action-objects
           (match len
             0 []
             1 items
             _  (lume.sort items action-sort) 1))

    (var taken-action false)
    (each [_ action (pairs action-objects)]
      (when (not taken-action)
        (let [outcome (action.action player)]
        (when outcome
          (db ["action taken" outcome])
          (set taken-action outcome)))))
    (when (not taken-action)
      (set taken-action (put-down-package player)))
    (local num (+ 1 (# player.packages)))
    (local points
           (match taken-action
               :take-pack (* num 5)
               :pickup-pack 0
               :post-office-pack (* num 10)
               :give-pack (* num 15)
               :put-down-pack -5
               _ 0
               ))
    (tset player :score
          (+ player.score
             points))
    (love.event.push :action taken-action points)

    ;; [{:has k :wants :post-office} {:has k :wants :post-office
    ;; {:number 1 :wants :post-offiice :has k}
  ))

(var last-flip 1)
(fn update-grass [player dt]
  (local grass player.grass)
  (local part grass.part)
  (if (= player.animation.animation-state :run)
      (part:setEmissionRate 3)
      (part:setEmissionRate 0)
      )
  (if (or (~= player.state-machine.current-state :walk)
          (~= last-flip player.flipped)
          )
      (part:reset)
      (part:start)
      )
  (set last-flip player.flipped)
  (part:setSizeVariation 0)
  (part:setSpeed (* 100 player.flipped))
  (part:setRotation 0)
  (part:setSpin 1)
  (part:setParticleLifetime 1 2)
  (part:setSizes 0 1 1 0)
  (part:setLinearAcceleration 0 5 0 10)
  ;; (part:setLinearAcceleration 0 0 0 0)
  (part:setSpread 0.5)
  (part:setLinearDamping 5)
  (part:setColors 1 1 1 1 0.5 0.5 1 0)
  (grass:update dt))

(fn check-level-over [self]
  (local state (require :state))
  (when (and state.level state.level.over)
    (self:lock-movement)
    (self:excited)))

(fn check-off-edge [player]
  (local state (require :state))
  (when (> player.pos.y 1000)
    (tset player.pos :x  player.reset.pos.x)
    (tset player.pos :y (- player.reset.pos.y 64))
    (state.world:update player player.pos.x player.pos.y)
    (tset player.speed :y 0)
    (tset player.speed :x 0)
    ))

(fn player.update [self dt]
  (local player-state (require :player-state))
  (local movement (require :player-movement))
  (let [[current-state [cols len]]
        ;; Identify colliding hit box
        (match (and self.movement self.state-machine.current-state)
          :fall [:fall (movement.movement-fall self dt)]
          :jump [:jump (movement.movement-jump self dt)]
          :walk [:walk (movement.movement-walk self dt)]
          false [self.state-machine.current-state [[] 0]]
          _ [:fall (movement.movement-walk self dt)]
          )]
    (local previous-state (if self.movement
                              (player-state self current-state dt)
                              self.state-machine.current-state))
    (local state self.state-machine.current-state)
    (local animation-state (animation-state-mapping self state previous-state))
    (local anim (. animations animation-state))
    (tset self.animation :previous-state self.animation.animation-state)
    (tset self.animation :animation-state animation-state)
    (tset self.animation :timer (+ dt self.animation.timer))
    (local timer self.animation.timer)
    (match [(= self.animation.animation-state :land)
            (= animation-state self.animation.previous-state)]
      [true true]
      (do
        (when (> (* 1000 timer) anim.rate)
            (tset self.animation :timer 0)
            (tset self.animation :index (+ self.animation.index 1))
            (when (> self.animation.index (# anim.frames))
              (tset self.animation :animation-state :walk)
              (tset self.animation :index 1))
            ))
      [false true]
      (do (when (> (* 1000 timer) anim.rate)
            (tset self.animation :timer 0)
            (tset self.animation :index (+ self.animation.index 1))
            (when (> self.animation.index (# anim.frames))
              (tset self.animation :index 1))))
      _
      (do
        (tset self.animation :timer 0)
        (tset self.animation :index 1))
      )
    (local f (direction self self.state-machine.current-state))
    (tset self :flipped f)
    (at-house? self)
    (update-grass self dt)
    (check-off-edge self)
    (check-level-over self)
    ))

(fn player.draw [self]
  (love.graphics.setColor 1 1 1 1)
  (local {: index : animation-state} self.animation)
  (local k (. (. animations animation-state) :frames index))
  (when (= :walk self.state-machine.current-state)
    ;; shadow
    (love.graphics.setColor 0.5 0.5 1 0.5)
    (love.graphics.draw self.backpack-image self.shadow-quad
                      (if (= 1 self.flipped)
                          (+ self.pos.x 9)
                          (+ self.pos.x 7))
                      (+ self.pos.y 17)))
  ;; particles
  (when (= self.state-machine.current-state :walk)
    (love.graphics.draw self.grass.part
                        (if (= 1 self.flipped) (+ self.pos.x 8) (+ self.pos.x 20))
                        (+ self.pos.y 31)))

  ;; backpack
  (when (> (# self.packages) 0)
    (love.graphics.setColor 0.8 0.8 0.8 1)
  (local backpack-quad (. self.backpack-quads (# self.packages)))
  (love.graphics.draw self.backpack-image backpack-quad
                      (if (= 1 self.flipped) (+ self.pos.x 10) (+ self.pos.x 22))
                      (+ self.pos.y -18)
                      0
                      self.flipped
                      1
                      ))

  ;; body
  (love.graphics.setColor 1 1 1 1)
  (local body-quad (. self.quads k ))
  (love.graphics.draw self.image body-quad
                      (if (= 1 self.flipped) self.pos.x (+ self.pos.x 32))
                      self.pos.y
                      0 self.flipped 1)

  ;;

  )

(local player-mt {:__index player
                :update player.update
                :collide player.collide
                :draw player.draw})

(fn player.lock-movement [player]
  (tset player :movement false))

(fn player.unlock-movement [player]
  (tset player :movement true))

(fn player.excited [player]
  (tset player :animation
        {:timer 0
         :frame-count 0
         :animation-state :fall-forward
         :previous-state :fall-forward
         :index 1
         :over true
         }))

(fn player.new [x y w h flipped]
  (local image assets.sprites.delivery-steve-player)
  (local loader (require :lib.loader))
  (local atlas (loader.load-atlas "assets/data/player" "assets/sprites"))
  (local sprite-atlas (loader.load-atlas "assets/data/sprite-data" "assets/sprites"))
  (local hosue-atlas (loader.load-atlas "assets/data/house-data" "assets/sprites"))

  ;; (fn new-quad [member]
  ;;   (let [{: x : y : w : h} (. atlas.param.frames member :frame)]
  ;;     (love.graphics.newQuad  x y w h 672 32)))
  (fn new-quad [member]
    (let [{: x : y : w : h} (. atlas.param.frames member :frame)]
      (love.graphics.newQuad  x y w h 672 32)))

  (local quads [])
  (for [ i 1 21]
    (tset quads i (new-quad i)))

    (fn new-quad2 [member]
      (let [{: x : y : w : h} (. sprite-atlas.param.frames member :frame)]
        (love.graphics.newQuad  x y w h 128 512)))
    (local backpack-quads [(new-quad2 :pack-1) (new-quad2 :pack-2)
                           (new-quad2 :pack-3) (new-quad2 :pack-4)])
    (local shadow-quad (new-quad2 :shadow))
    (local smoke-module (require :smoke))
    (local grass (smoke-module.new hosue-atlas :particle-c-4
                                   {:x (+ x 8) :y (+ y 32)}
                                   :grass))

  (local player {:pos {: x : y}
                 :speed {:x 0 :y 0}
                 :size {: w : h}
                 :col {:x 14 :y 22 :w 4 :h 4}
                 :x x :y y ;; origional position
                 :angle 0
                 : grass
                 : shadow-quad
                 :action nil
                 :type :player
                 :image image
                 :backpack-image (. sprite-atlas :image)
                 :quads quads
                 :flipped (or flipped 1)
                 :backpack-quads backpack-quads
                 :movement true
                 :animation {:timer 0
                             :frame-count 0
                             :animation-state :idle
                             :previous-state :idle
                             :index 1
                             :over true
                             }
                 :controler {:jump
                             {:timer
                              (timer params.player-speed.jump.timer)
                              :count 0
                              :released true
                              }
                             }
                 :active true
                 :score 0
                 :reset {:pos {:x x :y y}}
                 :packages []
                 :state-machine {:current-state :walk}
                 })
  (setmetatable player player-mt))

player
