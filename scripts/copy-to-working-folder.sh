#!/bin/bash

echo $0
echo $1
s=$1
filename="${s##*/}"
# aseprite -b --split-layers $1 --save-as ~/Development/love/apocalypse-meow/assets/sprites/"${filename%.*}".png

## aseprite -b --split-layers $1 --filename-format '{path}/{title}-{layer}.{extension}' --save-as ~/Development/love/apocalypse-meow/assets/sprites/"${filename%.*}".png

GAME="downsheep"

spritedir=~/Development/love/$GAME/assets/sprites/
datadir=~/Development/love/$GAME/assets/data/

aseprite -b \
         --all-layers \
         --ignore-layer Background \
         --split-layers \
         $1 \
         --filename-format '{path}/{title}.{extension}' \
         --sheet $spritedir"${filename%.*}".png \
         --sheet-type rows \
         --data  $datadir"${filename%.*}".json \
         --format json-array \
         --list-tags
