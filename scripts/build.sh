#!/bin/bash

VERSION=0.2.3

make love
mv releases/* tools/love.js
make cleansrc
cd tools/love.js
love-js delivery-steve-0.2.3.love change -t "Sample" -c
cp index.html change/index.html
cp love.css change/theme/love.css
cp consolewrapper.js change/consolewrapper.js

love-js delivery-steve-0.2.3.love perf -t "Sample"
cp index.html perf/index.html
cp love.css perf/theme/love.css
cp consolewrapper.js perf/consolewrapper.js

zip -r  -q change change/
mv change.zip delivery-steve-web-$VERSION.zip
#butler push --userversion $VERSION delivery-steve-web-$VERSION.zip alexjgriffith/delivery-steve:web
## scp -r change/ alexjgriffith:~/nginx/resources/vid/ds
## scp -r perf/ alexjgriffith:~/nginx/resources/vid/ds
# scp -r release/ alexjgriffith:~/nginx/resources/vid/ds
cd ../../
